import glob
import os

import numpy as np
import pandas as pd
import plotly
import plotly.graph_objs as go

from logger import MyLogger


class Report(MyLogger):
    """
    Devise data reporting script for reporting and summarizing the key metrics
    (active users,
    Vol for recipient type by corridor, # of transaction,
    Average transaction size per user, # of transaction per users, etc)
     The actual data file look a bit different but you have the idea from the attached xls.
    """

    def __init__(self):
        super().__init__()
        self.df = None
        self.interesting_columns = ["UUID", "Sender", 'Pay-in Currency', 'USD Equivalent',
                                    'Payout Currency', "CORRIDOR", "Segments", "Date Completion", "Sender Country",
                                    "OTC"]
        self.report_folder = 'output/report'
        self.graph_folder = 'graph'
        self.churn_duration_days = 180
        self.filter_corridor = 'BTC BUYING'  # 'BTC BUYING' None

    def report(self):
        self.get_data()
        self.read_combined_data()
        self.set_params()
        self.month_table()
        self.total_compare_graph()

        self.segment_table()
        self.total_segments_graph()

        self.country_table()
        self.total_country_compare_graph()

        self.country_segments_table()

        self.otc_table()
        self.otc_graph()

        # based on combined data.xlsx
        if self.combined_data is not None:
            self.calc_churn()
            self.active_users_graph_url = \
                self.graph_churn(columns=['Active users', 'New users'],
                                 title="Active users",
                                 filename='active_users_graph.html'
                                 )
            self.churn_graph_url = \
                self.graph_churn(columns=['Churn'],
                                 title="Churn with {} days duration"
                                 .format(self.churn_duration_days),
                                 filename='churn_graph.html',
                                 text_format='{:,.1f}%'
                                 )
            # top10 corridors
            self.corridor_table_html = self.corridor_table()
            self.total_corridor_graph_url = self.corridor_chart()

            # BTC BUYING
            self.corridor_buying_table_html = self.corridor_table(cor_filter='BTC BUYING')
            self.total_corridor_buying_graph_url = self.corridor_chart(filename='total_corridor_buying_graph.html',
                                                                       title="BTC Buying Corridor")

            # BTC SELLING
            self.corridor_selling_table_html = self.corridor_table(cor_filter='BTC SELLING')
            self.total_corridor_selling_graph_url = self.corridor_chart(filename='total_corridor_selling_graph.html',
                                                                        title='BTC Selling Corridor')

        self.save_to_file()

    def read_combined_data(self):
        try:
            self.logger.debug('Read from input/Combined Data.xlsx, sheetname=Sheet1')
            comb_df = pd.read_excel('input/Combined Data.xlsx', sheetname='Sheet1')
            # comb_df = pd.read_csv('input/Combined Data.csv')
            self.logger.debug('Reading finish')
        except:
            self.logger.error('Cant read input/Combined Data.xlsx')
            self.combined_data = None
            return False

        comb_df['Date Completion'] = pd.to_datetime(comb_df['Date'], errors='coerce')
        comb_df = comb_df[comb_df['Date Completion'] > '2014-01']
        comb_df['USD Margin'] = comb_df['USD Margin'].fillna(0)
        comb_df['USD Margin'] = comb_df['USD Margin'].apply(pd.to_numeric, errors='coerce')
        comb_df['CORRIDOR'] = comb_df['CORRIDOR'].str.replace(' : ', ':').fillna('')
        comb_df['CORRIDOR'] = comb_df['CORRIDOR'].str.replace(':', ' : ')
        comb_df['BUY/SELL'] = comb_df['BUY/SELL'].str.strip()
        comb_df = comb_df[comb_df['CORRIDOR'].str.contains(':')]
        self.combined_data = comb_df

    def set_params(self):
        pd.options.display.float_format = '{:,.0f}'.format
        self.df.loc[:, "Date Completion"] = pd.to_datetime(self.df['Date Completion'])
        otc_dict = {0: 'Automated', 1: 'OTC'}
        self.df['OTC'] = self.df['OTC'].map(otc_dict)
        self.df = self.df.rename(columns={"OTC": 'Source'})

        daily = self.df.groupby(by=self.df['Date Completion'].dt.date).sum().reset_index()
        # daily.head()
        daily.loc[:, "Date Completion"] = pd.to_datetime(daily['Date Completion'])
        self.daily = daily
        try:
            self.last_month = list(self.df['Date Completion'].dt.strftime('%Y-%m').unique())[-1]
        except:
            self.last_month = list(self.df['Date Completion'].dt.strftime('%Y-%m').unique())[-2]
        self.count_months = len(list(self.df['Date Completion'].dt.strftime('%Y-%m').unique()))

    def get_data(self, filename=None, filenames=None):
        """
        Read data from filename or filenames, if both is blank - then read direcotory input
        :param filename: the name of file for read info
        :param filenames: the list of names of files for read info
        :return:
        """
        # print(118, pd.read_csv('output/cleaned/2017-06.csv'))
        if filename is None and filenames is None:
            filenames = self.get_filenames()
        elif filename:
            filenames = [filename]

        for filename in filenames:
            self.read_file(filename=filename)

        self.df = self.df[self.interesting_columns]

        # shorter segment names
        segments = {"AFRICAN BUSINESSES TRADING BEYOND THEIR BORDERS (OUTBOUND FLOW)": "AFRICAN BUSINESSES",
                    "HIGH VOLUME BITCOIN TRADERS IN AFRICA": "BITCOIN TRADERS",
                    "INTERNATIONAL BUSINESSES TRADING WITH AFRICA (INBOUND FLOW)": "INTERNATIONAL BUSINESSES",
                    "REMITTANCE/PAYMENT COMPANIES SENDING TO/FROM AFRICA (INBOUND FLOW)": "REMITTANCE COMPANIES"}
        for old_seg, new_seg in segments.items():
            self.df = self.df.replace(old_seg, new_seg)

        # Get country names
        # country_df = pd.read_csv('data/country_codes.csv')
        # country_df = country_df[['name', 'alpha-2']]
        # country_df.columns = ["Country Name", "Sender Country"]
        # self.df = pd.merge(self.df, country_df, on="Sender Country", how="left")
        # null_time_out = df['Time Paid Out'].isnull()
        # df = df[~null_time_out]
        def choose_currency(c):
            cur = {c['Pay-in Currency'], c['Payout Currency']}
            other_currency = ['USD', 'EUR', 'GBP']
            cur = cur - set(['BTC'] + other_currency)
            if not cur:
                print(c)
            return list(cur)[0]

        self.df.loc[:, 'Country Name'] = self.df.apply(choose_currency, axis=1)

    def get_filenames(self):
        """
        Read  'Input' directory and get names of csv files
        :return: list of *.csv files
        """
        self.logger.info("Search *.csv files in input folder")
        filenames = []
        directory = os.path.join('output', 'cleaned')
        os.chdir(directory)
        for filename in glob.glob("*.csv"):
            filenames.append(os.path.join(directory, filename))
        os.chdir("../../")
        self.logger.info("found files: {}".format(filenames))
        return filenames

    def read_file(self, filename='data/dc.csv'):
        """
        Read file and concat info to self.df
        :param filename:
        :return:
        """
        self.logger.info('Reading from file {}'.format(filename))
        if '.csv' in filename:
            df = pd.read_csv(filename, index_col=False)
            self.logger.info("shape - {}".format(df.shape))
            if self.df is None:
                self.df = df
            else:
                self.df = pd.concat([self.df, df])
        elif '.xls' in filename:
            self.df = pd.read_excel(filename)
            self.sender_column = "Sender.1"
        self.df['index'] = np.arange(len(self.df))
        self.df = self.df.set_index('index')
        self.logger.debug('Reading finished, shape - {}'.format(self.df.shape))

    def month_table(self):
        df = self.df
        self.logger.info("prepare total_compare_df")

        rep = df.groupby(by=[df["Date Completion"].dt.strftime('%Y-%m')]).agg([np.sum, np.mean])
        rep["Daily Average"] = round(
            self.daily.groupby(by=[self.daily["Date Completion"].dt.strftime('%Y-%m')])["USD Equivalent"].mean(),
            2)
        rep.columns = ["Total Vol.", "Average Transaction", "Daily Average"]

        rep["Average Transaction"] = round(rep["Average Transaction"], 2)
        rep["MoM"] = rep['Total Vol.'].pct_change() * 100

        rep = rep[["Total Vol.", "MoM", "Average Transaction", "Daily Average"]]
        rep = rep.replace(np.nan, '', regex=True)
        self.total_compare_df = rep
        self.total_compare_html = self.table_html_prepare(self.total_compare_df)

    def compare_table(self, df, agg, group="Sender Country"):
        nan_index = 1
        res_df = df.groupby(by=[group]).sum()
        prev_month = None
        for month in df["Date Completion"].dt.strftime('%Y-%m').unique():
            column = df[df["Date Completion"].dt.strftime('%Y-%m') == month].groupby(by=[group])[
                "USD Equivalent"].agg(agg)
            # print(column)
            if prev_month and self.count_months > 1:
                col_name = 'MoM.{}'.format(nan_index)
                res_df[col_name] = round(100 * (column - res_df[prev_month]) / res_df[prev_month])
                res_df[col_name] = res_df[col_name].fillna(0).astype(int).astype(str) + '%'
                nan_index += 1
            res_df[month] = column
            prev_month = month
        res_df = res_df.drop(['USD Equivalent'], axis=1)
        res_df.fillna('')
        return res_df

    def country_segments_table(self):
        pt = self.df.pivot_table(values=['USD Equivalent'], index=['Segments', 'Country Name', 'Sender'],
                                 columns=pd.to_datetime(self.df['Date Completion']).dt.strftime("%Y-%m"))
        pt = pt['USD Equivalent']
        pt = pt.reset_index().sort_values(by=['Segments', 'Country Name', self.last_month],
                                          ascending=(True, True, False))

        if self.count_months > 1:
            # Calc mom
            cols = list(pt.columns)
            last_month = cols[-1]
            prev_month = cols[-2]
            pt['MoM'] = round(100 * (pt[last_month] - pt[prev_month]) / pt[prev_month]).fillna(0).astype(str) + '%'

            # move MoM to left
            cols.insert(len(cols) - 1, 'MoM')
            pt = pt[cols]

        # reindex
        pt = pt.set_index(['Segments', 'Country Name', 'Sender'], append=False, inplace=False)

        self.country_segments_table_html = self.table_html_prepare(pt)

    def compare_table_common(self, group="Sender Country"):
        df = self.df
        daily_segments = df.groupby(by=[df['Date Completion'].dt.date, df[group]]).sum().reset_index()
        daily_segments["Date Completion"] = pd.to_datetime(daily_segments['Date Completion'])

        count_trans = self.compare_table(df, np.count_nonzero, group)
        cols = list(count_trans.columns)

        m_d = {
            "Count transactions": count_trans,
            "Daily Average": self.compare_table(daily_segments, np.mean, group),
            "Total Sum": self.compare_table(df, np.sum, group),
        }
        rep = pd.concat(m_d.values(), axis=1, keys=m_d.keys())
        # print(rep)
        # print(cols)
        # df_ordered_multi_cols = pd.DataFrame(rep, columns=cols)
        return rep

    def segment_table(self):
        self.logger.info('Prepare segment table')
        segment_df = self.compare_table_common(group="Segments")
        segment_df = segment_df.replace('nan%', '', regex=True)  # remove nan%
        segment_df = segment_df.fillna('')  # remove nan
        # segment_df = segment_df.stack(0)  # move columns like 'sum' to rows
        self.segment_df = segment_df
        self.segment_html = self.table_html_prepare(segment_df)

    def country_table(self):
        self.logger.info('Prepare country table')
        c_df = self.compare_table_common(group="Country Name")
        c_df = c_df.sort_values([('Total Sum', self.last_month)], ascending=False)
        c_df = c_df.replace('nan%', '', regex=True)  # remove nan%
        c_df = c_df.fillna('')  # remove nan
        c_df.stack(1)
        self.country_df = c_df
        self.country_html = self.table_html_prepare(c_df)

    def table_html_prepare(self, df):
        return df.to_html().replace('<table border="1" class="dataframe">',
                                    '<table class="table table-striped table-condensed table-hover table-bordered">')

    def annotations_for_horizontal_bar(self, x_list, y_list):
        annotations = [
            dict(
                x=xi,
                y=yi,
                xref='x',
                yref='y',
                text='$' + '{:,.0f}'.format(xi),
                showarrow=False,
                xanchor='right',
                font=dict(
                    family='sans serif',
                    size=15,
                    color='#fff'
                )
            ) for xi, yi in zip(x_list, y_list)
        ]
        return annotations

    def total_compare_graph(self):
        # from plotly.offline import iplot
        self.logger.info("prepare total_compare_graph")

        # init_notebook_mode(connected=True)
        print(322, self.total_compare_df)
        ren = self.total_compare_df.reset_index()
        print(ren)
        data = [go.Bar(
            y=pd.to_datetime(ren['Date Completion']).dt.strftime('%B'),
            x=ren["Total Vol."],
            orientation='h',
        )]

        # Annotation
        layout = go.Layout(
            title='Total vol. compare',
            annotations=self.annotations_for_horizontal_bar(
                x_list=ren["Total Vol."], y_list=pd.to_datetime(ren['Date Completion']).dt.strftime('%B')
            )
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_compare_graph.html'

        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.total_compare_graph_url = os.path.join(self.graph_folder, filename)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))

        # annotationsList = [dict(
        #     x=xi,
        #     y=yi,
        #     text=str(yi),
        #     xanchor='right',
        #     yanchor='bottom',
        #     showarrow=False,
        # ) for xi, yi in zip(xcoord, ycoord)]

    def total_segments_graph(self):
        self.logger.info('Prepare segments graph')
        df = self.df
        mon_df = df.groupby(by=[df['Date Completion'].dt.strftime('%Y-%m'), df['Segments']]).sum().reset_index()
        mon_df.loc[:, "Date Completion"] = pd.to_datetime(mon_df['Date Completion'])
        data = []
        for seg in mon_df['Segments'].unique():
            mdf = mon_df[mon_df["Segments"] == seg]
            data.append(go.Bar(
                x=pd.to_datetime(mdf['Date Completion']).dt.strftime('%B'),
                y=mdf["USD Equivalent"],
                name=seg
            ))

        months = list(mon_df['Date Completion'].dt.strftime('%B').unique())
        segments = list(mon_df['Segments'].unique())

        gap = 1 / (len(segments) + 1)
        layout = go.Layout(
            barmode='group',
            title='Total Segments compare',
            annotations=[
                dict(
                    x=months.index(xi) + segments.index(seg) * gap - gap - 0.07,
                    y=yi,
                    xref='x',
                    yref='y',
                    text='$' + '{:,.0f}'.format(yi),
                    showarrow=False,
                    textangle=-90,
                    xanchor='right',
                    yanchor='bottom',
                    font=dict(
                        family='sans serif',
                        size=15,
                        color='#000'
                    )
                ) for xi, yi, seg in
                zip(mon_df['Date Completion'].dt.strftime('%B'), mon_df["USD Equivalent"], mon_df["Segments"])

            ]
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_segments_compare_graph.html'
        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))
        self.total_segments_graph_url = os.path.join(self.graph_folder, filename)

    def total_country_compare_graph(self):
        self.logger.info('Prepare country graph')
        df = self.df
        cdf = df.pivot_table(index=['Country Name'], columns=df["Date Completion"].dt.strftime('%Y-%m'), aggfunc=np.sum)

        cdf = cdf.sort_values(by=[("USD Equivalent", self.last_month)], ascending=False)
        # get only first 5 country
        short = cdf  # .iloc[:5, :]
        short = short.fillna(0)
        # short = short.replace('', 0, regex=True)
        # sum others
        # short.loc['Others'] = cdf.iloc[6:, :].sum()

        # prepare for make graph
        short = short["USD Equivalent"]
        short = short.unstack()
        short = short.reset_index()
        short.loc[:, "Date Completion"] = pd.to_datetime(short['Date Completion'])
        short.columns = ['Date Completion', 'Country Name', 'USD Equivalent']

        data = []
        for seg in short['Country Name'].unique():
            mdf = short[short['Country Name'] == seg]
            data.append(go.Bar(
                x=pd.to_datetime(mdf['Date Completion']).dt.strftime('%B'),
                y=mdf["USD Equivalent"],
                name=seg
            ))

        months = list(short['Date Completion'].dt.strftime('%B').unique())
        segments = list(short['Country Name'].unique())
        ann_df = short[short['USD Equivalent'] > 0]
        gap = 1 / (len(segments) + 1)
        layout = go.Layout(
            barmode='group',
            title='Total Country compare',
            annotations=[
                dict(
                    x=months.index(xi) + segments.index(seg) * gap - gap - 0.15,
                    y=yi,
                    xref='x',
                    yref='y',
                    text='$' + '{:,.0f}'.format(yi),
                    showarrow=False,
                    textangle=-90,
                    xanchor='right',
                    yanchor='bottom',
                    font=dict(
                        family='sans serif',
                        size=15,
                        color='#000'
                    )
                ) for xi, yi, seg in
                zip(ann_df['Date Completion'].dt.strftime('%B'), ann_df["USD Equivalent"], ann_df["Country Name"])

            ]
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_country_graph2.html'
        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))
        self.total_country_graph_url = os.path.join(self.graph_folder, filename)

    def calc_daily_churn(self):
        self.logger.info('Prepare daily churn')
        df = self.combined_data
        churn = df.groupby(by=[df["Date Completion"].dt.strftime('%Y-%m-%d'), df['Sender']])[
            'USD Margin'].sum().reset_index()
        churn['Date'] = pd.to_datetime(churn['Date Completion'])

        cols = [
            "Date",
            "Active users",
            "New users",
            "Lost users",
            "Churn"
        ]
        #
        prev_active = set()
        # res['Churn'] = res['Churn'].astype('float64')
        lines = []
        for month in churn["Date"].unique():
            prev = (month - np.timedelta64(self.churn_duration_days, 'D'))
            churn_selector = (churn['Date'] <= month) & (churn['Date'] >= prev)
            active = set(churn[churn_selector]['Sender'])
            line = {
                "Date": month,
                "Active users": len(active),
                "New users": len(active - prev_active),
                "Lost users": len(prev_active - active)
            }
            line['Churn'] = float(100 * line['Lost users'] / len(prev_active) if prev_active else 0.0)
            prev_active = active
            lines.append(line)

        res = pd.DataFrame(lines, columns=cols)
        return res

    def calc_churn(self):
        churn_daily_df = self.calc_daily_churn()
        self.logger.info('Prepare monthly churn')
        pd.options.display.float_format = '{:,.1f}'.format

        self.daily_churn_html = self.table_html_prepare(churn_daily_df)
        act = {
            "Active users": ['last'],
            "New users": ['sum'],
            "Lost users": ['sum'],
            "Churn": ['sum']
        }
        monthly_churn = churn_daily_df.groupby(by=churn_daily_df['Date'].dt.strftime("%Y-%m")).agg(act)
        mon = pd.DataFrame()
        mon['Active users'] = monthly_churn['Active users']['last']
        mon['New users'] = monthly_churn['New users']['sum']
        mon['Lost users'] = monthly_churn['Lost users']['sum']
        mon['Churn'] = monthly_churn['Churn']['sum']
        self.monthly_churn_df = mon
        self.monthly_churn_html = self.table_html_prepare(mon)
        pd.options.display.float_format = '{:,.0f}'.format

    def corridor_table(self, cor_filter=None):
        self.logger.info('Prepare corridor table')
        df = self.combined_data
        month = list(df['Date Completion'].dt.strftime('%Y-%m').unique())[-2]

        cor = df
        # groupby sender/month
        if cor_filter:
            self.logger.info('Filtering combined data for corridor, use only BUY/SELL={}'.format(cor_filter))
            cor = df[df['BUY/SELL'] == cor_filter]

        cor = cor.groupby(by=[cor['Date Completion'].dt.strftime('%Y-%m'), cor['CORRIDOR'], cor['Sender']])[
            'USD Margin'].sum()
        print(cor.head())
        cor = cor.reset_index()

        # get mean for senders
        cor = cor.groupby(by=[pd.to_datetime(cor['Date Completion']).dt.strftime('%Y-%m'), cor['CORRIDOR']]).mean()
        cor.reset_index()
        print(cor.head())
        show = cor.pivot_table(columns='Date Completion', index='CORRIDOR', values='USD Margin')
        show = show.fillna(0).sort_values(month, ascending=False)
        self.cor = cor
        return self.table_html_prepare(show)

    def corridor_chart(self, show_corridors='top', filename='total_corridor_graph.html', title='CORRIDOR'):
        self.logger.info('Prepare corridor chart, %s' % filename)
        crr = self.cor
        top_corridors = list(
            crr.reset_index().groupby(by='CORRIDOR')['USD Margin'].sum().sort_values(ascending=False)[:10].index)
        graph_cor = crr.reset_index()

        graph_cor = graph_cor.sort_values(by="Date Completion", ascending=False)

        data = []
        corridors = top_corridors if show_corridors == 'top' else graph_cor['CORRIDOR'].unique()
        for seg in corridors:
            mdf = graph_cor[graph_cor["CORRIDOR"] == seg]
            data.append(go.Scatter(
                x=pd.to_datetime(mdf['Date Completion']).dt.strftime('%Y-%m'),
                y=mdf["USD Margin"],
                name=seg,
                mode='lines+markers',
            ))
        # print(data)
        layout = go.Layout(
            title=title,
        )

        fig = go.Figure(data=data, layout=layout)

        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))
        return os.path.join(self.graph_folder, filename)

    def save_to_file(self):
        if self.combined_data is not None:
            combined_part_html = '''
             <h1> Based on input/Combined Data.xlsx</h1>
                    <div class="row">
                    <div class="col-md-12">
                    <h2 id='churn_table'>Churn with ''' + str(self.churn_duration_days) + ''' days duration and Active users</h2>
                    ''' + self.monthly_churn_html + '''
                    </div>
                    <div class="col-md-12">
                    <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                    src="''' + self.active_users_graph_url + '''"></iframe>
                    </div>
                    <div class="col-md-12">
                    <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                    src="''' + self.churn_graph_url + '''"></iframe>
                    </div>
                    
                    <div class="col-md-12">
                    <h2 id='arpu_corridors'>ARPU Corridors</h2>
                    ''' + self.corridor_table_html + '''
                    </div>
                    <div class="col-md-12">
                    <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                    src="''' + self.total_corridor_graph_url + '''"></iframe>
                    </div>   
                     <h2 id='buying_corridors'>BUYING Corridors</h2>
                    ''' + self.corridor_buying_table_html + '''
                    </div>
                    <div class="col-md-12">
                    <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                    src="''' + self.total_corridor_buying_graph_url + '''"></iframe>
                    </div>   
                             
                    <h2 id='selling_corridors'>SELLING Corridors</h2>
                    ''' + self.corridor_selling_table_html + '''
                    </div>
                    <div class="col-md-12">
                    <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                    src="''' + self.total_corridor_selling_graph_url + '''"></iframe>
                    </div>   
                                         
            '''
        else:
            combined_part_html = """
                         <h1> Based on input/Combined Data.xlsx</h1>
                         Cant read file input/Combined Data.xlsx
                         """

        html_string = '''
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                    <script src="https://getbootstrap.com/dist/js/bootstrap.min.js"></script>

                <style>body{ margin:0 100; background:whitesmoke; g.legend{opacity: 0.8};</style>
            </head>
            <body>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                                    aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Report</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">Tables <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#total_val">Total Val Compare</a></li>
                                        <li><a href="#source_compare">Source compare</a></li>
                                        <li><a href="#segments_compare">Segments Compare</a></li>
                                        <li><a href="#country_compare">Country compare</a></li>
                                        <li><a href="#country_segments_compare">Country Segments compare</a></li>
                                        <li><a href="#churn_table">Churn Rate</a></li>
                                        <li><a href="#arpu_corridors">ARPU corridors</a></li>
                                        <li><a href="#selling_corridors">Selling Corridors</a></li>
                                        <li><a href="#buying_corridors">Buying Corridors</a></li>
                                    </ul>
                                </li>
                
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
                <div class="container" style='margin-top:30px'>

                <h1 id="total_val">Total val compare</h1>
                <div class="row">
                <div class="col-md-6">
                ''' + self.total_compare_html + '''
                </div>
                <div class="col-md-6">
                <iframe id='val_chart' width="100%" height="300" frameborder="0" seamless="seamless" scrolling="no" \
        src="''' + self.total_compare_graph_url + '''"></iframe>
                </div>
                </div>
                
                <h2 id="source_compare">Source compare</h1>
                <div class="row">
                <div class="col-md-12">
                ''' + self.otc_html + '''
                </div>
                <div class="col-md-12">
                <iframe width="100%" height="300" frameborder="0" seamless="seamless" scrolling="no" \
        src="''' + self.otc_graph_url + '''"></iframe>
                </div>
                </div>
                
                <div class="row">
                <div class="col-md-12">
                <h2 id="segments_compare">Segments Compare</h2>
                ''' + self.segment_html + '''
                </div>
                <div class="col-md-12">
                <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                src="''' + self.total_segments_graph_url + '''"></iframe>
                </div>
                
                <div class="row">
                <div class="col-md-12">
                <h2 id='country_compare'>Country Compare</h2>
                ''' + self.country_html + '''
                </div>
                <div class="col-md-12">
                <iframe width="100%" height="550" frameborder="0" seamless="seamless" scrolling="no" \
                src="''' + self.total_country_graph_url + '''"></iframe>
                </div>
                </div>
                
                <div class="row">
                <div class="col-md-12">
                <h2 id='country_segments_compare'>Country Segments table</h2>
                ''' + self.country_segments_table_html + '''
                </div>
                </div>
                
                ''' + combined_part_html + '''
                </div>
            </body>
        </html>'''

        # Finally, write the html string to a local file.

        # In[185]:

        f = open('output/report/report.html', 'w')
        f.write(html_string)
        f.close()
        import webbrowser
        url = 'file://' + os.path.abspath('output/report/report.html')
        webbrowser.open(url)

    def graph_churn(self, columns=['Active users', 'New users'], title="Active users",
                    filename='active_users_graph.html', text_format='{:,.0f}'):
        g_df = self.monthly_churn_df.stack(0).reset_index().fillna(0)
        g_df.columns = ['Date', 'param', 'value']
        g_df = g_df[g_df['param'].isin(columns)]

        data = []
        for seg in columns:
            mdf = g_df[g_df["param"] == seg]
            data.append(go.Bar(
                x=pd.to_datetime(mdf['Date']).dt.strftime('%Y-%m'),
                y=mdf["value"],
                name=seg
            ))

        # annonation
        g_df['Date'] = pd.to_datetime(g_df['Date'])
        months = list(g_df['Date'].dt.strftime('%B').unique())
        gap = 1 / (len(columns) + 1)
        layout = go.Layout(
            barmode='group',
            title=title,
            # annotations=[
            #     dict(
            #         x=months.index(xi) + columns.index(col) * gap - 0.13,
            #         y=yi,
            #         xref='x',
            #         yref='y',
            #         text=text_format.format(yi),
            #         showarrow=False,
            #         xanchor='right',
            #         yanchor='bottom',
            #         font=dict(
            #             family='sans serif',
            #             size=19,
            #             color='#000'
            #         )
            #     ) for xi, yi, col in
            #     zip(g_df['Date'].dt.strftime('%B'), g_df["value"], g_df['param'])
            # ]
        )

        #     print(xi, yi)
        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))
        return os.path.join(self.graph_folder, filename)

    def otc_table(self):
        pd.options.display.float_format = '{:,.0f}'.format
        c_df = self.compare_table_common(group="Source")
        c_df = c_df.sort_values([('Total Sum', self.last_month)], ascending=False)
        c_df = c_df.replace('nan%', '', regex=True)  # remove nan%
        c_df = c_df.fillna('')  # remove nan
        self.otc_html = self.table_html_prepare(c_df)

    def otc_graph(self):
        df = report.df
        otc = df.groupby(by=[df.Source, pd.to_datetime(df['Date Completion']).dt.strftime('%Y-%m')])[
            'USD Equivalent'].sum()
        graph_otc = otc.reset_index()
        pd.options.display.float_format = '{:,.0f}'.format
        graph_otc = graph_otc.sort_values(by="Date Completion", ascending=False)
        data = []
        for seg in graph_otc['Source'].unique():
            mdf = graph_otc[graph_otc["Source"] == seg]
            data.append(go.Bar(
                y=pd.to_datetime(mdf['Date Completion']).dt.strftime('%B'),
                x=mdf["USD Equivalent"],
                name=seg,
                orientation='h',
            ))

        x_list = list(graph_otc["USD Equivalent"])
        for i, el in enumerate(x_list[1::2]):
            x_list[i * 2 + 1] += x_list[i * 2]

        layout = go.Layout(
            barmode='relative',
            title="Source",
            annotations=self.annotations_for_horizontal_bar(
                x_list=x_list,
                y_list=pd.to_datetime(graph_otc['Date Completion']).dt.strftime('%B')
            )
        )

        filename = 'otc_graph.html'

        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename=os.path.join(self.report_folder, self.graph_folder, filename),
                            show_link=False, auto_open=False)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))

        self.otc_graph_url = os.path.join(self.graph_folder, filename)


if __name__ == '__main__':
    # print('asdf, arg - {}'.format(sys.argv[1:]))
    report = Report()
    report.report()

