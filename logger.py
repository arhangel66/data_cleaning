import logging


class MyLogger():
    def __init__(self):
        self.logger = self.start_logging()


    def start_logging(self):
        self.stop_logging()
        if not hasattr(self, 'logger'):
            logger = logging.getLogger()
            logger.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(levelname)-8s [%(asctime)s] %(message)s')
            fh = logging.FileHandler('log_filename.txt')
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(formatter)
            logger.addHandler(fh)
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)
            ch.setFormatter(formatter)
            logger.addHandler(ch)
            logger.debug('Logging started')
            return logger
        return self.logger

    def stop_logging(self):
        if hasattr(self, 'logger'):
            print('try to stop logging')
            logger = self.logger
            logger.setLevel(logging.WARNING)
            fh = logging.FileHandler('log_filename.txt')
            logger.removeHandler(fh)
            fh.close()

            ch = logging.StreamHandler()
            logger.removeHandler(ch)