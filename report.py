import glob
import os
import traceback

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.plotly as py

from logger import MyLogger


def touchopen(filename, *args, **kwargs):
    # Open the file in R/W and create if it doesn't exist. *Don't* pass O_TRUNC
    fd = os.open(filename, os.O_RDWR | os.O_CREAT)

    # Encapsulate the low-level file descriptor in a python file object
    return os.fdopen(fd, *args, **kwargs)


def save_df_to_worksheet(writer, df, sheet_name, image_url=None):
    col = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    # col = 'WWSCaT6P8IioHvpQNJ6v'
    full_coluns = []
    for first in ' ' + col[:5]:
        for sec in col:
            full_coluns.append("{}{}".format(first, sec).strip())
    if df is not None:
        df.to_excel(writer, sheet_name)

        row_count, col_count = df.shape

        if image_url:
            if not isinstance(image_url, list):
                image_urls = [image_url]
            else:
                image_urls = image_url

            for i, image_url in enumerate(image_urls):
                if row_count < 30:
                    address = 'G{}'.format(row_count + 4 + i * 27)
                else:
                    col_len = len(df.columns)
                    address = '{}{}'.format(full_coluns[col_len + 2], 3 + i * 27)

                writer.sheets[sheet_name].insert_image(address, image_url)
    else:
        print('{} is blank'.format(sheet_name))


def show_diff(column):
    #     if int(column[-1]) != 0 and int(column[-2]) != 0:
    try:
        return round(float(column[-1]) - float(column[-2]), 2)
    except:
        return ''


# def show_diff_p(column):


class Report(MyLogger):
    """
    Devise data reporting script for reporting and summarizing the key metrics
    (active users,
    Vol for recipient type by corridor, # of transaction,
    Average transaction size per user, # of transaction per users, etc)
     The actual data file look a bit different but you have the idea from the attached xls.
    """
    SHEET_MONTH_TABLE = 'Total val compare'
    SHEET_SEGMENT_TABLE = 'Segments Compare'
    SHEET_COUNTRY_TABLE = 'Country Compare'
    SHEET_COUNTRY_SEGMENT_TABLE = 'Country Segments table'
    SHEET_OTC_TABLE = 'Source compare'
    SHEET_CHURN = 'Churn overal'
    SHEET_CORIDOR_TABLE = 'ARPU Corridors'
    SHEET_CORIDOR_BUYING_TABLE = 'BUYING Corridors'
    SHEET_CORIDOR_SELLING_TABLE = 'SELLING Corridors'
    ALL_SHEETS = [SHEET_MONTH_TABLE, SHEET_SEGMENT_TABLE, SHEET_COUNTRY_TABLE, SHEET_COUNTRY_SEGMENT_TABLE,
                  SHEET_OTC_TABLE, SHEET_CHURN,
                  SHEET_CORIDOR_TABLE,
                  SHEET_CORIDOR_BUYING_TABLE, SHEET_CORIDOR_SELLING_TABLE]

    def __init__(self):
        super().__init__()
        self.df = None
        self.interesting_columns = ["UUID", "Sender", 'Pay-in Currency', 'MTM USD',
                                    'Payout Currency', "CORRIDOR", "Segments", "Date Started", "Sender Country",
                                    "OTC"]
        self.report_folder = 'output/report'
        self.graph_folder = 'graph'
        self.churn_duration_days = 180
        self.filter_corridor = 'BTC BUYING'  # 'BTC BUYING' None
        self.py = py.sign_in('arhangel6', 'WWSCaT6P8IioHvpQNJ6v')
        self.result = {sheet: {'name': sheet, 'df': None, 'image': None} for sheet in self.ALL_SHEETS}
        self.combined_data = None

        # example res = {'name': "Total compare', 'df': ..., 'image': ..}

    def report(self):
        self.get_data()
        self.read_combined_data()
        # self.combined_data = None
        self.set_params()
        self.month_table()
        self.total_compare_graph()
        self.segment_table()
        self.total_segments_graph()

        self.country_table()
        self.total_country_compare_graph()

        self.country_segments_table()

        self.otc_table()
        self.otc_graph()

        # based on combined data.xlsx
        if self.combined_data is not None:
            # churn overal
            self.churn_overal(sheet_name=self.SHEET_CHURN, df=self.combined_data)

            # other churn
            self.combined_data['SOURCE'] = self.combined_data['SOURCE'].str.lower().str.strip()
            self.combined_data['App'] = self.combined_data['App'].str.lower().str.strip()
            self.combined_data['PRODUCT'] = self.combined_data['PRODUCT'].str.lower().str.strip()
            churn_list = [
                {
                    "selector": (self.combined_data['PRODUCT'] == 'enterprise'),
                    "sheet_name": 'Churn product Eneterprise',
                },
                {
                    "selector": (self.combined_data['PRODUCT'] != 'enterprise'),
                    "sheet_name": 'Churn product Trade pay',
                },
                {
                    "selector": (self.combined_data['App'].isin(['bitpesa v2', 'bitpesa'])),
                    "sheet_name": 'Churn API',
                },
                {
                    "selector": (~self.combined_data['App'].isin(['bitpesa v2', 'bitpesa']))
                                & (self.combined_data['SOURCE'] == 'otc'),
                    "sheet_name": 'Churn OTC',
                },
                {
                    "selector": (~self.combined_data['App'].isin(['bitpesa v2', 'bitpesa']))
                                & (self.combined_data['SOURCE'] == 'trans.csv'),
                    "sheet_name": 'Churn Trans Csv',
                },
            ]

            for line in churn_list:
                self.churn_overal(
                    sheet_name=line['sheet_name'],
                    df=self.combined_data[line['selector']]
                )
                self.ALL_SHEETS.append(line['sheet_name'])

            # top10 corridors
            self.result[self.SHEET_CORIDOR_TABLE]['df'] = self.corridor_table()
            self.result[self.SHEET_CORIDOR_TABLE]['image'] = self.corridor_chart()

            # BTC BUYING
            self.result[self.SHEET_CORIDOR_BUYING_TABLE]['df'] = self.corridor_table(cor_filter='BTC BUYING')
            self.result[self.SHEET_CORIDOR_BUYING_TABLE]['image'] = self.corridor_chart(
                filename='total_corridor_buying_graph.png',
                title="BTC Buying Corridor")

            # BTC SELLING
            self.result[self.SHEET_CORIDOR_SELLING_TABLE]['df'] = self.corridor_table(cor_filter='BTC SELLING')
            self.result[self.SHEET_CORIDOR_SELLING_TABLE]['image'] = self.corridor_chart(
                filename='total_corridor_selling_graph.png',
                title='BTC Selling Corridor')
        self.save_to_file()

    def save_chart_image(self, fig, full_filename):
        try:
            py.image.save_as(fig, filename=full_filename)
            # pass
        except:
            self.logger.error('Cant save image to {}'.format(full_filename))

    def churn_overal(self, sheet_name, df):
        self.result[sheet_name] = {'name': sheet_name, 'df': None, 'image': None}
        image_ending = sheet_name[-5:]
        mon_df = self.calc_churn(sheet_name=sheet_name, df=df)
        self.result[sheet_name]['image'] = [
            self.graph_churn(
                columns=['Active users', 'New users'],
                title="Active users",
                filename='active_users_graph_{}.png'.format(image_ending),
                monthly_df=mon_df,
            ),
            self.graph_churn(
                columns=['Churn'],
                title="Churn with {} days duration".
                    format(self.churn_duration_days),
                filename='churn_graph_{}.png'.format(image_ending),
                text_format='{:,.1f}%',
                monthly_df=mon_df,
            )
        ]

    @classmethod
    def is_read_xls(cls):
        """
        if comb_size
        :return:
        """
        path = os.path.join('input', 'Combined Data.xlsx')
        s_comb_size = str(os.path.getsize(path))

        path = os.path.join('input', 'comb_size.txt')

        with touchopen(path, 'r+') as f:
            cur_size = f.read()

        csv_path = os.path.join('output', 'margin_csv', 'Combined Data_c.csv')
        b_csv_exist = os.path.isfile(csv_path)

        if b_csv_exist and cur_size == s_comb_size:
            return False
        return True

    @classmethod
    def save_combined_csv(cls, comb_df):
        s_comb_csv_path = os.path.join('output', 'margin_csv', 'Combined Data_c.csv')
        # save as csv
        comb_df.to_csv(s_comb_csv_path)

        # update comb_size.txt
        path = os.path.join('input', 'Combined Data.xlsx')
        s_comb_size = str(os.path.getsize(path))

        path = os.path.join('input', 'comb_size.txt')
        with open(path, 'w') as f:
            f.write(s_comb_size)

    def read_combined_data(self):

        # try:
        if self.is_read_xls():
            self.logger.debug('reading combined xls')
            comb_df = pd.read_excel('input/Combined Data.xlsx', sheetname='Sheet1')
            self.logger.debug('reading combined xls as csv for future')
            self.save_combined_csv(comb_df)
        else:
            # read_combined_csv
            comb_df = pd.read_csv('output/margin_csv/Combined Data_c.csv', sep=',', decimal='.', encoding="ISO-8859-1")
        # self.logger.debug('Read from input/Combined Data.csv')

        # comb_df = pd.read_excel('input/Combined Data.xlsx', sheetname='Sheet1')
        # comb_df = pd.read_csv('input/Combined Data.csv', sep=';', decimal=",", encoding="ISO-8859-1") # error_bad_lines=False)  #
        self.logger.debug('Reading finish - {}'.format(comb_df.shape))
        comb_df = comb_df.rename(columns=lambda x: x.strip())
        comb_df['Date Started'] = pd.to_datetime(comb_df['Date'], errors='coerce', infer_datetime_format=True)
        comb_df = comb_df[comb_df['Date Started'] > '2014-01']
        comb_df['USD Margin'] = comb_df['USD Margin'].fillna(0)
        comb_df['USD Margin'] = comb_df['USD Margin'].apply(pd.to_numeric, errors='coerce')
        comb_df['CORRIDOR'] = comb_df['CORRIDOR'].str.replace(' : ', ':').fillna('')
        comb_df['CORRIDOR'] = comb_df['CORRIDOR'].str.replace(':', ' : ')
        comb_df['BUY/SELL'] = comb_df['BUY/SELL'].str.strip()
        comb_df = comb_df[comb_df['CORRIDOR'].str.contains(':')]
        self.logger.debug('Reading finish - {}'.format(comb_df.shape))
        # comb_df.to_csv('output/cleaned/{}'.format('combined_cleaned.csv'))
        # self.logger.debug('saved to csv')
        # writer = pd.ExcelWriter('output/cleaned/combined_cleaned.xlsx', engine='xlsxwriter')
        # comb_df.to_excel(writer, 'Sheet 1')
        # self.logger.debug('saved to xlsx')

        self.combined_data = comb_df

    def set_params(self):
        pd.options.display.float_format = '{:,.0f}'.format
        self.df.loc[:, "Date Started"] = pd.to_datetime(self.df['Date Started'])
        otc_dict = {0: 'Automated', 1: 'OTC'}
        self.df['OTC'] = self.df['OTC'].map(otc_dict)
        self.df = self.df.rename(columns={"OTC": 'Source'})
        self.df = self.df.sort_values(by='Date Started')
        self.months = self.df['Date Started'].dt.strftime('%Y-%m').unique()

        daily = self.df.groupby(by=self.df['Date Started'].dt.date).sum().reset_index()
        # daily.head()
        daily.loc[:, "Date Started"] = pd.to_datetime(daily['Date Started'])
        self.daily = daily
        self.last_month = self.df['Date Started'].dt.strftime('%Y-%m').max()
        self.count_months = len(self.months)
        self.prev_month = None
        if self.count_months > 1:
            self.prev_month = self.months[-2]

    def get_data(self, filename=None, filenames=None):
        """
        Read data from filename or filenames, if both is blank - then read direcotory input
        :param filename: the name of file for read info
        :param filenames: the list of names of files for read info
        :return:
        """
        if filename is None and filenames is None:
            filenames = self.get_filenames()
        elif filename:
            filenames = [filename]

        for filename in filenames:
            self.read_file(filename=filename)

        self.df = self.df[self.interesting_columns]

        # shorter segment names
        segments = {"AFRICAN BUSINESSES TRADING BEYOND THEIR BORDERS (OUTBOUND FLOW)": "AFRICAN BUSINESSES",
                    "HIGH VOLUME BITCOIN TRADERS IN AFRICA": "BITCOIN TRADERS",
                    "INTERNATIONAL BUSINESSES TRADING WITH AFRICA (INBOUND FLOW)": "INTERNATIONAL BUSINESSES",
                    "REMITTANCE/PAYMENT COMPANIES SENDING TO/FROM AFRICA (INBOUND FLOW)": "REMITTANCE COMPANIES"}

        for old_seg, new_seg in segments.items():
            self.df = self.df.replace(old_seg, new_seg)

        null_date_started = self.df['Date Started'].isnull()
        self.logger.debug("removing rows with Date Started is Null - {}".format(self.df[null_date_started].shape[0]))
        self.df = self.df[~null_date_started]

        # Get country names
        # country_df = pd.read_csv('data/country_codes.csv')
        # country_df = country_df[['name', 'alpha-2']]
        # country_df.columns = ["Country Name", "Sender Country"]
        # self.df = pd.merge(self.df, country_df, on="Sender Country", how="left")
        def choose_currency(c):
            cur = {c['Pay-in Currency'], c['Payout Currency']}
            other_currency = ['USD', 'EUR', 'GBP']
            cur = cur - set(['BTC'] + other_currency)
            try:
                return list(cur)[0] if cur else ''
            except:
                traceback.print_exc()
                # print(cur)
                return ''

        self.df.loc[:, 'Country Name'] = self.df.apply(choose_currency, axis=1)

    def get_filenames(self):
        """
        Read  'Input' directory and get names of csv files
        :return: list of *.csv files
        """
        self.logger.info("Search *.csv files in input folder")
        filenames = []
        directory = os.path.join('output', 'margin_csv')
        os.chdir(directory)
        # for filename in glob.glob("margin_*.xlsx"):
        for filename in glob.glob("margin_*.csv"):
            # if len(filename.split('-')) == 2:
            filenames.append(os.path.join(directory, filename))
        os.chdir("../../")
        self.logger.info("found files: {}".format(filenames))
        return filenames

    def read_file(self, filename='data/dc.csv'):
        """
        Read file and concat info to self.df
        :param filename:
        :return:
        """
        self.logger.info('Reading from file {}'.format(filename))
        if '.csv' in filename:
            df = pd.read_csv(filename, index_col=False)
        elif '.xls' in filename:
            df = pd.read_excel(filename, sheetname='Cleaned Data')
            self.sender_column = "Sender.1"

        self.logger.info("shape - {}".format(df.shape))
        if self.df is None:
            self.df = df
        else:
            self.df = pd.concat([self.df, df])

        self.df['index'] = np.arange(len(self.df))
        self.df = self.df.set_index('index')
        self.logger.debug('Reading finished, shape - {}'.format(self.df.shape))

    # @profile
    def month_table_old(self):
        df = self.df
        self.logger.info("prepare total_compare_df")

        rep = df.groupby(by=[df["Date Started"].dt.strftime('%Y-%m')])['MTM USD'].agg([np.sum, np.mean])

        rep["Daily Average"] = round(
            self.daily.groupby(by=[self.daily["Date Started"].dt.strftime('%Y-%m')])["MTM USD"].mean(),
            2)
        rep.columns = ["Total Vol.", "Average Transaction", "Daily Average"]

        rep["Average Transaction"] = round(rep["Average Transaction"], 2)
        rep["MoM"] = rep['Total Vol.'].pct_change() * 100

        rep = rep[["Total Vol.", "MoM", "Average Transaction", "Daily Average"]]
        rep = rep.replace(np.nan, '', regex=True)
        self.logger.info('finish month_table')
        return rep

    def month_table(self):
        rep = self.month_table_old()
        lines = [
            {
                'Report': 'Volume',
                'Total($)': rep['Total Vol.'][-1],
                'Average per transaction': rep['Average Transaction'][-1],
                'Daily Average': rep['Daily Average'][-1]
            }, {
                'Report': 'Margin',
                'Total($)': show_diff(rep['Total Vol.']),
                'Average per transaction': show_diff(rep['Average Transaction']),
                'Daily Average': show_diff(rep['Daily Average'])
            }]
        res = pd.DataFrame(lines)
        res = res[['Report', 'Total($)', 'Average per transaction', 'Daily Average']]
        self.result[self.SHEET_MONTH_TABLE + '_old'] = {'df': rep}
        self.result[self.SHEET_MONTH_TABLE]['df'] = res

    def compare_table(self, df, agg, group="Sender Country"):
        nan_index = 1
        res_df = df.groupby(by=[group]).sum()
        prev_month = None
        for month in df["Date Started"].dt.strftime('%Y-%m').unique():
            column = df[df["Date Started"].dt.strftime('%Y-%m') == month].groupby(by=[group])[
                "MTM USD"].agg(agg)
            if prev_month and self.count_months > 1:
                col_name = 'MoM.{}'.format(nan_index)
                res_df[col_name] = round(100 * (column - res_df[prev_month]) / res_df[prev_month])
                res_df[col_name] = res_df[col_name].fillna(0).astype(int).astype(str) + '%'
                nan_index += 1
            res_df[month] = column
            prev_month = month
        res_df = res_df.drop(['MTM USD'], axis=1)
        res_df.fillna('')
        return res_df

    def country_segments_table(self):
        pt = self.df.pivot_table(values=['MTM USD'], index=['Segments', 'Country Name', 'Sender'],
                                 columns=pd.to_datetime(self.df['Date Started']).dt.strftime("%Y-%m"))
        pt = pt['MTM USD']
        pt = pt.reset_index().sort_values(by=['Segments', 'Country Name', self.last_month],
                                          ascending=(True, True, False))

        if self.count_months > 1:
            # Calc mom
            cols = list(pt.columns)
            last_month = cols[-1]
            prev_month = cols[-2]
            pt['MoM'] = round(100 * (pt[last_month] - pt[prev_month]) / pt[prev_month]).fillna(0).astype(str) + '%'

            # move MoM to left
            cols.insert(len(cols) - 1, 'MoM')
            pt = pt[cols]

        # reindex
        pt = pt.set_index(['Segments', 'Country Name', 'Sender'], append=False, inplace=False)

        self.result[self.SHEET_COUNTRY_SEGMENT_TABLE]['df'] = pt

    def compare_table_common(self, group="Sender Country"):
        df = self.df
        daily_segments = df.groupby(by=[df['Date Started'].dt.date, df[group]]).sum().reset_index()
        daily_segments["Date Started"] = pd.to_datetime(daily_segments['Date Started'])

        count_trans = self.compare_table(df, np.count_nonzero, group)
        cols = list(count_trans.columns)

        m_d = {
            "Count transactions": count_trans,
            "Daily Average": self.compare_table(daily_segments, np.mean, group),
            "Total Sum": self.compare_table(df, np.sum, group),
        }
        rep = pd.concat(m_d.values(), axis=1, keys=m_d.keys())
        # df_ordered_multi_cols = pd.DataFrame(rep, columns=cols)
        return rep

    def segment_table_old(self):
        self.logger.info('Prepare segment table')
        segment_df = self.compare_table_common(group="Segments")
        segment_df = segment_df.replace('nan%', '', regex=True)  # remove nan%
        segment_df = segment_df.fillna(0)  # remove nan
        # segment_df = segment_df.stack(0)  # move columns like 'sum' to rows
        self.result[self.SHEET_SEGMENT_TABLE + '_old'] = {'df': segment_df}
        return segment_df

    def segment_table(self):
        s_df = self.segment_table_old()['Total Sum']
        s_df = s_df.replace('', 0)

        cols = s_df.columns
        last_month = cols[-1]
        if len(cols) > 2:
            prev_month = cols[-3]
            diff_p = cols[-2]
            s_df['Margin($)'] = s_df[last_month].astype('float64') - s_df[prev_month].astype('float64')
            new_df = s_df[[last_month, 'Margin($)', diff_p]]
            new_df.columns = ['Vol($)', 'Margin($)', '% Margin']
        else:
            new_df = s_df[[last_month]]
            new_df.columns = ['Vol($)']
        self.result[self.SHEET_SEGMENT_TABLE]['df'] = new_df

    def country_table(self):
        self.logger.info('Prepare country table')
        c_df = self.compare_table_common(group="Country Name")
        c_df = c_df.sort_values([('Total Sum', self.last_month)], ascending=False)
        c_df = c_df.replace('nan%', '', regex=True)  # remove nan%
        c_df = c_df.fillna('')  # remove nan
        c_df.stack(1)
        self.result[self.SHEET_COUNTRY_TABLE]['df'] = c_df

    def table_html_prepare(self, df):
        return df.to_html().replace('<table border="1" class="dataframe">',
                                    '<table class="table table-striped table-condensed table-hover table-bordered">')

    def annotations_for_horizontal_bar(self, x_list, y_list):
        annotations = [
            dict(
                x=xi,
                y=yi,
                xref='x',
                yref='y',
                text='$' + '{:,.0f}'.format(xi) if xi else '',
                showarrow=False,
                xanchor='right',
                font=dict(
                    family='sans serif',
                    size=15,
                    color='#fff'
                )
            ) for xi, yi in zip(x_list, y_list)
        ]
        return annotations

    def total_compare_graph(self):
        # from plotly.offline import iplot
        self.logger.info("prepare total_compare_graph")

        # init_notebook_mode(connected=True)
        ren = self.result[self.SHEET_MONTH_TABLE + '_old']['df'].reset_index()
        data = [go.Bar(
            y=pd.to_datetime(ren['Date Started']).dt.strftime('%B'),
            x=ren["Total Vol."],
            orientation='h',
        )]

        # Annotation
        layout = go.Layout(
            title='Total vol. compare',
            annotations=self.annotations_for_horizontal_bar(
                x_list=ren["Total Vol."], y_list=pd.to_datetime(ren['Date Started']).dt.strftime('%B')
            )
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_compare_graph.png'
        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)
        self.result[self.SHEET_MONTH_TABLE]['image'] = full_filename
        self.logger.info('save graph to file - {}'.format(full_filename))

        # annotationsList = [dict(
        #     x=xi,
        #     y=yi,
        #     text=str(yi),
        #     xanchor='right',
        #     yanchor='bottom',
        #     showarrow=False,
        # ) for xi, yi in zip(xcoord, ycoord)]

    def total_segments_graph(self):
        self.logger.info('Prepare segments graph')
        df = self.df
        mon_df = df.groupby(by=[df['Date Started'].dt.strftime('%Y-%m'), df['Segments']]).sum().reset_index()
        mon_df.loc[:, "Date Started"] = pd.to_datetime(mon_df['Date Started'])
        data = []
        for seg in mon_df['Segments'].unique():
            mdf = mon_df[mon_df["Segments"] == seg]
            data.append(go.Bar(
                x=pd.to_datetime(mdf['Date Started']).dt.strftime('%B'),
                y=mdf["MTM USD"],
                name=seg
            ))

        months = list(mon_df['Date Started'].dt.strftime('%B').unique())
        segments = list(mon_df['Segments'].unique())

        gap = 1 / (len(segments) + 1)
        layout = go.Layout(
            barmode='group',
            title='Total Segments compare',
            annotations=[
                dict(
                    x=months.index(xi) + segments.index(seg) * gap - gap - 0.07,
                    y=yi,
                    xref='x',
                    yref='y',
                    text='$' + '{:,.0f}'.format(yi),
                    showarrow=False,
                    textangle=-90,
                    xanchor='right',
                    yanchor='bottom',
                    font=dict(
                        family='sans serif',
                        size=15,
                        color='#000'
                    )
                ) for xi, yi, seg in
                zip(mon_df['Date Started'].dt.strftime('%B'), mon_df["MTM USD"], mon_df["Segments"])

            ]
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_segments_compare_graph.png'
        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)
        self.result[self.SHEET_SEGMENT_TABLE]['image'] = full_filename
        self.logger.info('save graph to file - {}'.format(full_filename))

    def total_country_compare_graph(self):
        self.logger.info('Prepare country graph')
        df = self.df
        cdf = df.pivot_table(index=['Country Name'], columns=df["Date Started"].dt.strftime('%Y-%m'), aggfunc=np.sum)

        cdf = cdf.sort_values(by=[("MTM USD", self.last_month)], ascending=False)
        # get only first 5 country
        short = cdf  # .iloc[:5, :]
        short = short.fillna(0)
        # short = short.replace('', 0, regex=True)
        # sum others
        # short.loc['Others'] = cdf.iloc[6:, :].sum()

        # prepare for make graph
        short = short["MTM USD"]
        short = short.unstack()
        short = short.reset_index()
        short.loc[:, "Date Started"] = pd.to_datetime(short['Date Started'])
        short.columns = ['Date Started', 'Country Name', 'MTM USD']

        data = []
        for seg in short['Country Name'].unique():
            mdf = short[short['Country Name'] == seg]
            data.append(go.Bar(
                x=pd.to_datetime(mdf['Date Started']).dt.strftime('%B'),
                y=mdf["MTM USD"],
                name=seg
            ))

        months = list(short['Date Started'].dt.strftime('%B').unique())
        segments = list(short['Country Name'].unique())
        ann_df = short[short['MTM USD'] > 0]
        gap = 1 / (len(segments) + 1)
        layout = go.Layout(
            barmode='group',
            title='Total Country compare',
            annotations=[
                dict(
                    x=months.index(xi) + segments.index(seg) * gap - gap - 0.15,
                    y=yi,
                    xref='x',
                    yref='y',
                    text='$' + '{:,.0f}'.format(yi),
                    showarrow=False,
                    textangle=-90,
                    xanchor='right',
                    yanchor='bottom',
                    font=dict(
                        family='sans serif',
                        size=15,
                        color='#000'
                    )
                ) for xi, yi, seg in
                zip(ann_df['Date Started'].dt.strftime('%B'), ann_df["MTM USD"], ann_df["Country Name"])

            ]
        )

        fig = go.Figure(data=data, layout=layout)
        filename = 'total_country_graph2.png'
        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)
        self.result[self.SHEET_COUNTRY_TABLE]['image'] = full_filename
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))

    def calc_daily_churn(self, df):
        self.logger.info('Prepare daily churn')
        churn = df.groupby(by=[df["Date Started"].dt.strftime('%Y-%m-%d'), df['Sender']])[
            'USD Margin'].sum().reset_index()
        churn['Date'] = pd.to_datetime(churn['Date Started'])

        cols = [
            "Date",
            "Active users",
            "New users",
            "Lost users",
            "Churn"
        ]
        #
        prev_active = set()
        # res['Churn'] = res['Churn'].astype('float64')
        lines = []
        for month in churn["Date"].unique():
            prev = (month - np.timedelta64(self.churn_duration_days, 'D'))
            churn_selector = (churn['Date'] <= month) & (churn['Date'] >= prev)
            active = set(churn[churn_selector]['Sender'])
            line = {
                "Date": month,
                "Active users": len(active),
                "New users": len(active - prev_active),
                "Lost users": len(prev_active - active)
            }
            line['Churn'] = float(100 * line['Lost users'] / len(prev_active) if prev_active else 0.0)
            prev_active = active
            lines.append(line)

        res = pd.DataFrame(lines, columns=cols)
        return res

    def calc_churn(self, sheet_name, df):
        churn_daily_df = self.calc_daily_churn(df)
        self.logger.info('Prepare monthly churn')
        pd.options.display.float_format = '{:,.1f}'.format

        self.daily_churn_html = self.table_html_prepare(churn_daily_df)
        act = {
            "Active users": ['last'],
            "New users": ['sum'],
            "Lost users": ['sum'],
            "Churn": ['sum']
        }
        churn_daily_df['Date'] = pd.to_datetime(churn_daily_df['Date'])
        monthly_churn = churn_daily_df.groupby(by=churn_daily_df['Date'].dt.strftime("%Y-%m")).agg(act)
        monthly_churn_df = pd.DataFrame()
        monthly_churn_df['Active users'] = monthly_churn['Active users']['last']
        monthly_churn_df['New users'] = monthly_churn['New users']['sum']
        monthly_churn_df['Lost users'] = monthly_churn['Lost users']['sum']
        monthly_churn_df['Churn'] = monthly_churn['Churn']['sum']
        # self.monthly_churn_df = mon
        self.result[sheet_name]['df'] = monthly_churn_df
        # self.monthly_churn_html = self.table_html_prepare(mon)
        pd.options.display.float_format = '{:,.0f}'.format
        return monthly_churn_df

    def corridor_table(self, cor_filter=None):
        # try:
        self.logger.info('Prepare corridor table')
        df = self.combined_data
        null_corridor = df['CORRIDOR'].isnull()
        self.logger.debug("removing rows with corridor is Null - {}".format(df[null_corridor].shape[0]))
        df = df[~null_corridor]

        month = list(df['Date Started'].dt.strftime('%Y-%m').unique())[-2]

        cor = df
        # groupby sender/month
        if cor_filter:
            self.logger.info('Filtering combined data for corridor, use only BUY/SELL={}'.format(cor_filter))
            cor = df[df['BUY/SELL'] == cor_filter]

        cor = cor.groupby(by=[cor['Date Started'].dt.strftime('%Y-%m'), cor['CORRIDOR'], cor['Sender']])[
            'USD Margin'].sum()
        cor = cor.reset_index()

        # get mean for senders
        cor = cor.groupby(by=[pd.to_datetime(cor['Date Started']).dt.strftime('%Y-%m'), cor['CORRIDOR']]).mean()
        cor = cor.reset_index()
        cor = cor.fillna(0)
        self.logger.info(cor.fillna(0).head(1))
        show = cor.fillna(0).pivot_table(columns='Date Started', index='CORRIDOR', values='USD Margin')
        # print(show.head())
        show = show.fillna(0).sort_values(month, ascending=False)
        self.cor = cor
        # except:
        #     self.logger.error('Some error, when calc corridor table')
        return show

    def corridor_chart(self, show_corridors='top', filename='total_corridor_graph.png', title='CORRIDOR'):
        self.logger.info('Prepare corridor chart, %s' % filename)
        crr = self.cor
        top_corridors = list(
            crr.reset_index().groupby(by='CORRIDOR')['USD Margin'].sum().sort_values(ascending=False)[:10].index)
        graph_cor = crr.reset_index()

        graph_cor = graph_cor.sort_values(by="Date Started", ascending=False)

        data = []
        corridors = top_corridors if show_corridors == 'top' else graph_cor['CORRIDOR'].unique()
        for seg in corridors:
            mdf = graph_cor[graph_cor["CORRIDOR"] == seg]
            data.append(go.Scatter(
                x=pd.to_datetime(mdf['Date Started']).dt.strftime('%Y-%m'),
                y=mdf["USD Margin"],
                name=seg,
                mode='lines+markers',
            ))
        layout = go.Layout(
            title=title,
        )

        fig = go.Figure(data=data, layout=layout)

        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)
        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))
        return full_filename

    def save_to_file(self):
        writer = pd.ExcelWriter('output/report/report.xlsx', engine='xlsxwriter')

        # Total compare
        for sheet in self.ALL_SHEETS:
            d_sheet = self.result[sheet]
            save_df_to_worksheet(writer, d_sheet['df'], d_sheet['name'], d_sheet['image'])

        writer.save()

    def graph_churn(self, columns=None, title="Active users",
                    filename='active_users_graph.png', text_format='{:,.0f}', monthly_df=None):
        if columns is None:
            columns = ['Active users', 'New users']
        g_df = monthly_df.stack(0).reset_index().fillna(0)
        g_df.columns = ['Date', 'param', 'value']
        g_df = g_df[g_df['param'].isin(columns)]

        data = []
        for seg in columns:
            if g_df.shape[0]:
                mdf = g_df[g_df["param"] == seg]
                data.append(go.Bar(
                    x=pd.to_datetime(mdf['Date']).dt.strftime('%Y-%m'),
                    y=mdf["value"],
                    name=seg
                ))

        # annonation
        g_df['Date'] = pd.to_datetime(g_df['Date'])
        months = list(g_df['Date'].dt.strftime('%B').unique())
        gap = 1 / (len(columns) + 1)
        layout = go.Layout(
            barmode='group',
            title=title,
        )

        fig = go.Figure(data=data, layout=layout)
        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)

        self.logger.info('save graph to file - {}'.format(full_filename))
        return full_filename

    def otc_table_old(self):
        pd.options.display.float_format = '{:,.0f}'.format
        c_df = self.compare_table_common(group="Source")
        c_df = c_df.sort_values([('Total Sum', self.last_month)], ascending=False)
        c_df = c_df.replace('nan%', '', regex=True)  # remove nan%
        c_df = c_df.fillna('')  # remove nan
        self.result[self.SHEET_OTC_TABLE]['df'] = c_df

    def otc_table(self):
        s_df = self.compare_table_common(group="Source")['Total Sum']
        cols = list(s_df.columns)
        last_month = cols[-1]
        if self.count_months > 1:
            prev_month = cols[-3]
            diff_p = cols[-2]
            s_df['Margin($)'] = s_df[last_month].astype('float64') - s_df[prev_month].astype('float64')
            new_df = s_df[[last_month, 'Margin($)', diff_p]]
            new_df.columns = ['Vol($)', 'Margin($)', '% Margin']
        else:
            new_df = s_df[[last_month]]
            new_df.columns = ['Vol($)']
        self.result[self.SHEET_OTC_TABLE]['df'] = new_df

    def otc_graph(self):
        df = self.df
        otc = df.groupby(by=[df.Source, pd.to_datetime(df['Date Started']).dt.strftime('%Y-%m')])[
            'MTM USD'].sum()
        graph_otc = otc.reset_index()
        pd.options.display.float_format = '{:,.0f}'.format
        graph_otc = graph_otc.sort_values(by="Date Started", ascending=False)
        data = []
        for seg in graph_otc['Source'].unique():
            mdf = graph_otc[graph_otc["Source"] == seg]
            data.append(go.Bar(
                y=pd.to_datetime(mdf['Date Started']).dt.strftime('%B'),
                x=mdf["MTM USD"],
                name=seg,
                orientation='h',
            ))

        x_list = list(graph_otc["MTM USD"])
        for i, el in enumerate(x_list[1::2]):
            x_list[i * 2 + 1] += x_list[i * 2]

        layout = go.Layout(
            barmode='relative',
            title="Source",
            annotations=self.annotations_for_horizontal_bar(
                x_list=x_list,
                y_list=pd.to_datetime(graph_otc['Date Started']).dt.strftime('%B')
            )
        )

        filename = 'otc_graph.png'

        fig = go.Figure(data=data, layout=layout)
        full_filename = os.path.join(self.report_folder, self.graph_folder, filename)
        self.save_chart_image(fig, full_filename)
        self.result[self.SHEET_OTC_TABLE]['image'] = full_filename

        self.logger.info(
            'save graph to file - {}'.format(os.path.join(self.report_folder, self.graph_folder, filename)))

        self.otc_graph_url = os.path.join(self.graph_folder, filename)


if __name__ == '__main__':
    # print('asdf, arg - {}'.format(sys.argv[1:]))
    report = Report()
    report.report()
