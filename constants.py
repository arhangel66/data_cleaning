segments_list = [
    {
        'senders': ['Adam Sani (ASA Oil & Gas)', "Africa's Talking", 'ahis world of games and satellite link nig.ltd',
                    'AstarBridge Limited', 'AVC MANAGEMENT COMPANY LIMITED', 'Azikel Group',
                    'Bamidele Ayemibo (3TIMPEX)', 'bitsoko kenya', 'Bitwala', 'BTcomms Nig Ltd',
                    'CABIPA (NIGERIA) LIMITED', 'Cafe Neo (Ngozi Dozie)', 'Ceecenta (Abel Obazee)',
                    "Charlie's Travels B.V.", 'Chemtech Group of Companies Limited', 'Christine Lott',
                    'Chrome Ace Concepts Limited', 'Deedsway Global Concepts', 'Dial Digital', 'Digital Encode Limted',
                    'Eleni Georgopoulou', 'Eskimi limited', 'Fort Solutions Ltd', 'G T Forex', 'GZ INDUSTRIES LIMITED',
                    'Interswitch Limited', 'Kentaro Nagai (Afric-Inc)', 'kou zina limited', 'Makarios Realtor Company',
                    'Masaka Creamery Limited', 'Mizami Nigeria Limited', 'MK Capital Resources, LLC', 'Mobi Hunter',
                    'Nest Group Africa Limited', 'NoWahala Tours/VIP Express Tourism Ltd', 'Planet Exchange Limited',
                    'Pluspeople Kenya Limited', 'Procter & Gamble Nigeria Ltd', 'Pula Advisors Limited', 'RTS Autoview',
                    'Savanna Sunrise Online Marketing Limited', 'SolarNow Services (U) Limited', 'Sproxil',
                    'SUNPETOK TECHNOLOGIES LIMITED', 'Surebids Limited', 'Swan Medico Limited', 'Terragon Limited',
                    'Thebe Wellness Services Nigeria', 'TRANSFER PRICING PARTNERS LTD', 'TRAVELSTART WEB LOGISTICS LTD',
                    'Twinpine Limited', 'Vesiri Ibru (Demaar Properties)', 'Village Energy',
                    'WebSimba Limited (EatOut Kenya)', 'Wesley Dean (Daproim)', 'Palmfox Internatinal (U) Ltd',
                    'cogelec energy SARL', 'SPIRITED FRONTIER TRADING LTD', 'Yujo Lounge Limited',
                    'IMI Mobile VAS Nigeria Limited', 'Koutix Global Enterprises',
                    'ASHTON AND DAVE TRAVELS AND HOLIDAYS LIMITED', 'The German Technology Nig Ltd',
                    'OUICARRY SENEGAL SARL', 'cogelec energy SARL', 'MANDARINE SARL', 'zympay nigeria limited',
                    'Procter & Gamble Nigeria Ltd (Active Account)', 'VIC DOMSTELL LIMITED',
                    'STRATON OAKMONT EQUITIES LIMITED','Tunapanda Kibera CBO','Refitok International Limited',
                    'Bitfnance(Pvt) Ltd', 'APPFISHERS SOFTINC TECHNOLOGY', 'Coingh Limited','THE CANDEL COMPANY LIMITED','Beyonic Uganda','Coingh Limited',"CARESVILLE PHARMACEUTICAL LIMITED's Company'"
                    ],
        'segment_name': 'AFRICAN BUSINESSES TRADING BEYOND THEIR BORDERS (OUTBOUND FLOW)'
    },
    {
        'senders': ['BeForward Ltd.', 'Bitbond Gmbh', 'Chui Media (Samuel Gachui)', 'Dotun Olowoproku (Starta)',
                    'Edwina Sercombe', 'Fabio Barrionuevo', 'Femi Esuruoso', 'Jon Rathauser - KEHEALA',
                    'Kj cargo/shipping services ltd', 'Mbabazi styles (Grace Byeitima)', 'Netcargo', 'OnePin',
                    'Orkid Studio (James Mitchell)', 'Premise Data', 'Soko', 'Stichting Butterfly Works',
                    'Wefarm (Amy Barthorpe)', 'United Nations Barber Studio & International Call Center',
                    'Pula Advisors GmbH', 'Exmoor Partners', 'CO2i (Trade name: DryGro)','Asoko Insight Limited','INTERNATIONAL PROCUREMENT & LOGISTICS CONSULTANTS LTD','arbitrage capital limited',
                    'Leaf II, SPC'],
        'segment_name': 'INTERNATIONAL BUSINESSES TRADING WITH AFRICA (INBOUND FLOW)'
    },
    {
        'senders': ['AFTAB CURRENCY EXCHANGE LIMITED', 'Bitwage Ireland Limited', 'Branch International',
                    'DusuPay Limited', 'Epiphyte', 'Exchange4free Ltd', 'Grants Payment Solutions', 'IDT',
                    'PING EXPRESS', 'SimbaPay', 'Transfer Zero', 'Zympay Limited', 'Mondial', 'Daytona',
                    'OKLink Technology Company Limited', 'Mondial','Mukuru','Moneytrans Payment Services S.A'],
        'segment_name': 'REMITTANCE/PAYMENT COMPANIES SENDING TO/FROM AFRICA (INBOUND FLOW)'
    }
]

rate_sheet_aps = {'Primary': {'sheet': 'Mondial'},
                  'TransferZero': {'sheet': 'TransferZero'},
                  'Remitpal': {'sheet': 'Grants Payments'},
                  'Exchange4Free': {'sheet': 'E4Free'},
                  'LycaRemit': {'sheet': 'Lyca Remit'},
                  'Mukuru': {'sheet': 'Mukuru'},
                  'Moneytrans Payment Services SA': {'sheet': 'MoneyTrans'},
                  }


africa_currency = ['UGX', 'NGN', 'TZS', 'KES', 'ZAR', 'CNY', 'XOF', 'GHS']
other_currency = ['USD', 'EUR', 'GBP']
