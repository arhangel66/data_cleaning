# coding=utf-8
import pandas as pd

from clean import CleanData
from constants import other_currency, africa_currency, rate_sheet_aps

full_currency_list = africa_currency + other_currency
"""
M

"""


def excel_style_col(col):
    """ Convert given row and column number to an Excel-style cell name. """
    result = []
    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    while col:
        col, rem = divmod(col - 1, 26)
        result[:0] = LETTERS[rem]
    return ''.join(result)


class MarginClean(CleanData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.margin_df = None  # DF of Margin Sheet
        self.second_leg_df = None  # df of Deleted Transactions sheet
        client_rate_is_null_selector = lambda df: (df['Client Rate Type'] == '0')
        self.non_african_to_african_selector = lambda df: (
            df['Pay-in Currency'].isin(other_currency) &
            df['Payout Currency'].isin(africa_currency)
        )
        self.african_to_non_african_selector = lambda df: (
            df['Pay-in Currency'].isin(africa_currency) &
            df['Payout Currency'].isin(other_currency)
        )
        self.african_to_african_selector = lambda df: (
            df['Pay-in Currency'].isin(africa_currency) &
            df['Payout Currency'].isin(africa_currency)
        )
        self.app_is_new_selector = lambda df: (
            df['App'].isin(rate_sheet_aps.keys())
        )
        self.bitpesa_selector = lambda df: (df['App'] == 'Bitpesa V2')
        self.fiat_selector = lambda df: (df['fiat'] == True)

    def double_fiat_fiat_not_IDT(self):
        base_df = self.df
        self.logger.info('base_df - {}'.format(base_df.shape))
        super().double_fiat_fiat_not_IDT()
        new_df = self.df
        second_leg = base_df[~base_df.index.isin(new_df.index)]
        self.second_leg_df = second_leg

    def get_data(self, *args, **kwargs):
        res = super().get_data(*args, **kwargs)
        self.df['Exchange Rate (BTC/USD) - Copy'] = self.df['Exchange Rate (BTC/USD)']
        return res

    def margin(self, testing=False, compare=False, only_one='', only_csv=False, **kwargs):
        """
        Main script of Margin class
        :return:
        """
        self.only_one = only_one
        self.only_csv = only_csv

        input_file = None  # None means auto search in 'input folder'
        fin_file = None
        if testing:
            input_file = 'input/margin_check/20171015_input2.xlsx'
            fin_file = 'input/margin_check/20171108_final.xlsm'

        self.clean(save_results=False, filename=input_file, **kwargs)


        # move exchange_copy to last pos: maybe move to other place of code
        cols = list(self.df.columns)
        cols.insert(len(cols), cols.pop(cols.index('Exchange Rate (BTC/USD) - Copy')))
        self.df = self.df[cols]


        # cleaned data sheet
        self.df = self.calc_client_rate(self.df)  # ok
        self.df = self.calc_client_usd(self.df)  # ok
        self.df = self.calc_mtm_rate(self.df)  # ok
        self.df = self.calc_mtm_usd(self.df)  # ok
        self.df = self.calc_margin_columns(self.df)  # ok
        self.df = self.calc_difference(self.df)

        #
        # deleted sheet
        self.logger.info('Deleted FIAT - FIAT transaction calculations')
        if self.second_leg_df is not None:
            self.second_leg_df = self.calc_client_rate(self.second_leg_df, calc_payin_eur=True)
            self.second_leg_df = self.calc_client_usd(self.second_leg_df, calc_payin_eur=True)
            self.second_leg_df = self.calc_mtm_rate(self.second_leg_df, calc_payin_eur=True)
            self.second_leg_df = self.calc_mtm_usd(self.second_leg_df, calc_payin_eur=True)
            self.second_leg_df = self.calc_margin_columns(self.second_leg_df, calc_payin_eur=True)
            self.second_leg_df = self.calc_difference(self.second_leg_df, calc_payin_eur=True)

        if testing and compare:
            self.margin_compare(fin_file)
        else:
            # remove test columns
            self.df = self.clean_columns(self.df)
            if self.second_leg_df is not None:
                self.second_leg_df = self.clean_columns(self.second_leg_df)

        # self.calc_margin_report()

        if not only_csv:
            self.save_results()
        self.save_csv()

    def calc_client_rate(self, df, calc_payin_eur=False):
        """
        # from last instruction
        =If(AND(Pay-in Currency="NGN";APP:APP="ACE");**get_exchange_rate from table**;
        If "APP" column = "Money Transfer" and "Payout currency" = NGN then vlookup for IDT rates
        ELSEIf "APP" column = "Money Transfer" and "Payout currency" = UGX then vlookup for IDT rates
        ELSEIF "Recipient Type" is not "BTC" then = ("Payout Amount" /"Pay-in Amount")/("Exchange Rate (BTC/USD)")
        ELSEIF "Recipient Type" = "BTC" then = ("Pay-in Amount"/"Payout Amount")/"Exchange Rate (BTC/USD)")
        ELSEIF "Recipient Type" = "BTC" AND "PAYIN" = EUR or GBP = 1/ ("Pay-in Amount"/"Payout Amount")/"Exchange Rate (BTC/USD)"))
        ELSEIF "Recipient Type" is NOT "BTC" AND "PAYOUT" = EUR or GBP = 1/ ("Payout Amount" /"Pay-in Amount")/("Exchange Rate (BTC/USD)")
        :param df:
        :param calc_payin_eur:


        # new instructions from 28.02.2018
        (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        1. If APP name = Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free then
            a.     client rate = look up the rates workbook.
         If app = Primary lookup rates for different dates in Mondial Sheet  column D
         If app = TransferZero lookup rates for different dates in TransferZero Sheet  column B
         If app = Remitpals lookup rates for different dates in Grants Payments Sheet  column B
         If app = Exchange4Free lookup rates for different dates in E4Free Sheet  column B
         If app = LycaRemit lookup rates for different dates in Lyca Remit Sheet  column B


         Second condition to add (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        2. If APP name = Bitpesa V2 then
        a.     client rate =
          If PAYIN amount > PAYOUT Amount, then  rate = Payin Amount / Payout Amount Else = Payout Amount / Pay in Amount

        Third condition to add (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        3. If APP name = Bitpesa V2 then
        a.     client rate =
        If PAYIN amount > PAYOUT Amount, then  rate = Payin Amount / Payout Amount Else = Payout Amount / Pay in Amount

        (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        4. If APP name = Bitpesa V2 then
        a.     client rate =
          If PAYIN amount > PAYOUT Amount, then  rate = Payin Amount / Payout Amount
            Else = Payout Amount / Pay in Amount


        # translate to code
        if FIAT TO FIAT:
            if (NON AFRICAN CURRENCY >> AFRICAN CURRENCY):
                if app in ['Primary', 'Transfer zero', 'Remitpal', 'LycaRemit', 'Exchange4Free']:
                    vlookup rates from separate sheets(Mondial Sheet, Grants Payments Sheet, etc)

            if app == 'Bitpesa v2':
                if PAYIN amount > PAYOUT amount:
                    rate = Payin Amount / Payout Amount
                else:
                    rate = Payout Amount / Pay in Amount

        with all other lines - use our previous code, right?


        :return:
        """
        self.logger.info('df')
        df = pd.merge(df, self.fiat_rates, on='Date Started', how='left')
        # writer = pd.ExcelWriter('output/margin/test.xls', engine='xlsxwriter')
        # df.to_csv('output/margin/test.csv')

        self.logger.info('Calc client Rate')
        # print(105, df.head())
        df['Client Rate Type'] = '0'
        df['Client Rate'] = '0'
        client_rate_is_null_selector = lambda df: (df['Client Rate Type'] == '0')

        # print(df.shape)
        # print(df[client_rate_is_null_selector].shape)
        # If "APP" column = "Money Transfer" and "Payout currency" = NGN then vlookup for IDT rates
        if not calc_payin_eur:
            selector = (
                client_rate_is_null_selector(df) & (df['App'] == 'ACE') & (df['Payout Currency'].isin(['NGN'])))
            df.loc[selector, 'Client Rate Type'] = 'ACE NGN rates'
            df.loc[selector, 'Client Rate'] = df['Exchange Rate (BTC/USD)']

        if calc_payin_eur:
            # ELSEIF "Recipient Type" = "BTC" AND "PAYIN" = EUR or GBP = 1/ ("Pay-in Amount"/"Payout Amount")/"Exchange Rate (BTC/USD)"))
            selector = (
                client_rate_is_null_selector(df) & (df['Recipient Type'] == 'BTC') & (
                    df['Pay-in Currency'].isin(['GBP', 'EUR'])))
            df.loc[selector, 'Client Rate Type'] = '1/(Intermediate/Payout/Rate)'
            df.loc[selector, 'Client Rate'] = 1 / ((df['Pay-in Amount'] / df['Payout Amount']) \
                                                   / df['Exchange Rate (BTC/USD) - Copy'])

            # ELSEIF "Recipient Type" is NOT "BTC" AND "PAYOUT" = EUR or GBP = 1/ ("Payout Amount" /"Pay-in Amount")/("Exchange Rate (BTC/USD)")
            selector = (
                client_rate_is_null_selector(df) & (df['Recipient Type'] != 'BTC') & (
                    df['Payout Currency'].isin(['GBP', 'EUR'])))
            df.loc[selector, 'Client Rate Type'] = '1/(Payout/Intermediate/Rate)'
            df.loc[selector, 'Client Rate'] = 1 / (df['Payout Amount'] / df['Pay-in Amount'] \
                                                   / df['Exchange Rate (BTC/USD) - Copy'])



        selector = (
            client_rate_is_null_selector(df) & (df['App'] == 'Money Transfer') & (df['Payout Currency'].isin(['NGN'])))
        df.loc[selector, 'Client Rate Type'] = 'IDT NGN rates'
        df.loc[selector, 'Client Rate'] = df['Exchange Rate (BTC/USD)']

        # If "APP" column = "Money Transfer" and "Payout currency" = UGX then vlookup for IDT rates
        selector = (
            client_rate_is_null_selector(df) & (df['App'] == 'Money Transfer') & (df['Payout Currency'].isin(['UGX'])))
        df.loc[selector, 'Client Rate Type'] = 'IDT UGX rates'
        df.loc[selector, 'Client Rate'] = df['Exchange Rate (BTC/USD)']

        # elif "Recipient Type" is not "BTC" then = ("Payout Amount" /"Pay-in Amount")/ ("Exchange Rate (BTC/USD)")
        selector = (client_rate_is_null_selector(df) & (df['Recipient Type'] != 'BTC'))
        df.loc[selector, 'Client Rate Type'] = 'Payout/Intermediate/Rate'
        df.loc[selector, 'Client Rate'] = df['Payout Amount'] / df['Pay-in Amount'] \
                                          / df['Exchange Rate (BTC/USD) - Copy']

        # ELSEIF "Recipient Type" = "BTC" then = ("Pay-in Amount"/"Payout Amount")/"Exchange Rate (BTC/USD)")
        selector = (client_rate_is_null_selector(df) & (df['Recipient Type'] == 'BTC'))
        df.loc[selector, 'Client Rate Type'] = 'Intermediate/Payout/Rate'
        df.loc[selector, 'Client Rate'] = df['Pay-in Amount'] / df['Payout Amount'] \
                                          / df['Exchange Rate (BTC/USD) - Copy']


        # print(df['Payout Amount'])
        # print(df['Pay-in Amount'])
        # print(df['Exchange Rate (BTC/USD) - Copy'])
        # print(df['Client Rate'])

        # 1500 / 0,0000919 / 3597,12

        # =IF(and(Payout Currency="NGN";APP:APP="ACE");ВПР(AT:AT;'All rates'!X:AB;5;ЛОЖЬ);
        # IF(and(Payout Currency="NGN";APP:APP="Money Transfer");ВПР(AT:AT;'All rates'!X:Z;2;ЛОЖЬ);
        # IF(and(Payout Currency="UGX";APP:APP="Money Transfer");ВПР(AT:AT;'All rates'!X:Z;3;ЛОЖЬ);
        # IF(Recipient Type<>"BTC";(R1295/P1295)/AY1295;(P1295/R1295)/AY1295))))

        """
        # translate to code
        if FIAT TO FIAT:
            if (NON AFRICAN CURRENCY >> AFRICAN CURRENCY):
                if app in ['Primary', 'Transfer zero', 'Remitpal', 'LycaRemit', 'Exchange4Free']:
                    vlookup rates from separate sheets(Mondial Sheet, Grants Payments Sheet, etc)
        """
        selector = (
            self.fiat_selector(df) &
            self.app_is_new_selector(df) &
            (
                self.african_to_african_selector(df) |
                self.non_african_to_african_selector(df)
            )
        )
        df.loc[selector, 'Client Rate Type'] = 'vlookup rates from separate sheets'

        def get_fiat_rate(c):
            column_name = "%s_%s/%s" % (c['App'], c['Pay-in Currency'], c['Payout Currency'])
            return c.get(column_name, '-1')

        df.loc[selector, 'Client Rate'] = df.apply(get_fiat_rate, axis=1)

        """
        if app == 'Bitpesa v2':
            if PAYIN amount > PAYOUT amount:
                rate = Payin Amount / Payout Amount
            else:
                rate = Payout Amount / Pay in Amount
        """
        bitpesa_selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            (df['Pay-in Amount'] > df['Payout Amount'])
        )
        df.loc[bitpesa_selector, 'Client Rate'] = df['Pay-in Amount'] / df['Payout Amount']
        df.loc[bitpesa_selector, 'Client Rate Type'] = 'Bitpesa Pay-in/Payout'

        bitpesa_selector2 = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            (df['Pay-in Amount'] < df['Payout Amount'])
        )
        df.loc[bitpesa_selector2, 'Client Rate'] = df['Payout Amount'] / df['Pay-in Amount']
        df.loc[bitpesa_selector2, 'Client Rate Type'] = 'Bitpesa Payout/Pay-in'

        zero = df[df['Client Rate'] == 0]
        if zero.shape[0]:
            self.logger.info('this has client rate is 0')
            self.logger.info(zero.head())
        df = df[~(df['Client Rate'] == 0)]

        return df

    def calc_client_usd(self, df, calc_payin_eur=False):
        """
        IF(Recipient Type<>"BTC";(Payout Amount/Client Rate);(Pay-in Amount/Client Rate))

        IF(AND(OR(Pay-in Currency="EUR";Pay-in Currency="GBP");Recipient Type="BTC");Client Rate*Pay-in Amount;
        IF(Recipient Type<>"BTC";(Payout Amount/Client Rate);(Pay-in Amount/Client Rate)))

        # from 20171015_final2
        =If(AND(OR(Pay-in Currency="EUR";Pay-in Currency="GBP");Recipient Type="BTC");Client Rate*Pay-in Amount;
        If(AND(OR(Payout Currency="EUR";Payout Currency="GBP");Recipient Type<>"BTC");Client Rate*Client Rate;
        If(Recipient Type<>"BTC";(Client Rate/Client Rate);(Pay-in Amount/Client Rate))))

        Client USD
        IF "Recipient Type" = "BTC" and "PAYIN" = EUR or GBP, then =  "Payout Amount"*"Client Rate"
        IF "Recipient Type" IS NOT "BTC" and "PAYOUT" = EUR or GBP, then =  "Pay-in Amount"*"Client Rate"
        IF "Recipient Type" = "BTC" then =  "Payout Amount"/"Client Rate"
        IF "Recipient Type" is NOT  "BTC" then =  "Pay-in Amount"/"Client Rate"

        New from 01 mar 2018
        Conditions to add (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free then
        b.    Client USD
        =If PAYIN = (GBP then Client rate * Pay-in Amount)/Payout Currency rate in MTM
        =If PAYIN = (EUR then Client Rate * Pay-in Amount)/Payout Currency rate in MTM
        =If PAYIN = (USD then  Client Rate * Pay-in Amount)/Payout Currency rate in MTM

        Second condition to add (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        2. If APP name = Bitpesa V2 then
        b.    Client USD
                =If PAYIN = (GBP then Client rate * Pay-in Amount)/Payout Currency rate in MTM
                =If PAYIN = (EUR then Client Rate * Pay-in Amount)/Payout Currency rate in MTM
                =If PAYIN = (USD then  Client Rate * Pay-in Amount)/Payout Currency rate in MTM

        Third condition to add (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        3. If APP name = Bitpesa V2 then
        b.    Client USD
                =If PAYIN = (NGN then Client rate * Pay Out Amount)/Payin Currency rate in MTM

        Fourth condition to add (AFRICAN CURRENCY >> AFRICAN CURRENCY)
        4. If APP name = Bitpesa V2 then
        b.    Client USD
              When Payin < Payout amount e.g. NGN  to UGX
                =If PAYIN = (NGN then Client rate * PayIn Amount)/Payout Currency rate in MTM
               When Payin > Payout amount  e.g NGN to ZAR
                =If PAYOUT = (ZAR then Client rate * Payout Amount)/Payin Currency rate in MTM

        IF APP name == 'Mukuru' then
            Payout Amount (Column K)/Client Rate (Col AD)


        my thoughts:
        If FIAT-TO-FIAT:
            if (NON AFRICAN CURRENCY >> AFRICAN CURRENCY) and (special apps or 'Bitpesa V2'):
                client usd = (Client rate * Pay-in Amount)/Payout Currency rate in MTM (vlookup from m2m_currency)

            if app == 'Bitpesa V2' and AFRICAN CURRENCY >> NON AFRICAN CURRENCY:
                client usd =  (Client rate * Pay Out Amount)/Payin Currency rate in MTM

            if app == 'bipesa v2' and (AFRICAN CURRENCY >> AFRICAN CURRENCY):
                if payin < payout:
                    client usd = (Client rate * PayIn Amount) / Payout Currency rate in MTM
                else:
                    client usd = (Client rate * Payout Amount)/Payin Currency rate in MTM


        :param self:
        :param df:
        :return:
        """
        self.logger.info('Calc Client USD column')
        df['Client USD'] = 0
        client_usd_is_null_selector = lambda df: (df['Client USD'] == '0')
        """
        my thoughts:
        If FIAT-TO-FIAT:
            if (NON AFRICAN CURRENCY >> AFRICAN CURRENCY) and (special apps or 'Bitpesa V2'):
                client usd = (Client rate * Pay-in Amount)/Payout Currency rate in MTM (vlookup from m2m_currency)
        """

        # calc payout rate and payin rate from mtm
        df = self.append_m2m_rates(df)

        # TODO move it to separate func for prepare_data
        def get_payout_currency(c):
            # print(c['Payout Currency'], float(c.get(, '-1')), c)
            # print(c['Payout Currency'])
            # print(c.get(c['Payout Currency'], '-1'))
            return c.get(c['Payout Currency'], '-1')

        # get_payout_currency = lambda df: df.get(df['Payout Currency'], '-1')
        get_payin_currency = lambda df: df.get(df['Pay-in Currency'], '-1')
        # print(376, df.apply(get_payout_currency, axis=1))
        df['Pay-in Rate'] = 1
        df['Payout Rate'] = 1
        df.loc[self.fiat_selector(df), 'Pay-in Rate'] = df.apply(get_payin_currency, axis=1)
        df.loc[self.fiat_selector(df), 'Payout Rate'] = df.apply(get_payout_currency, axis=1)

        df.loc[(df['Client USD'] == 0) & (df['Recipient Type'] != 'BTC'), 'Client USD'] = df['Payout Amount'] / df[
            'Client Rate']

        df.loc[(df['Recipient Type'] == 'BTC') & (df['Client USD'] == 0), 'Client USD'] = \
            df['Pay-in Amount'] / df['Client Rate']

        if calc_payin_eur:
            # as in instruction
            df.loc[(df['Client USD'] == 0) & (df['Recipient Type'] == 'BTC') & (
                df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'Client USD'] = \
                df['Pay-in Amount'] * df['Client Rate']

            df.loc[(df['Client USD'] == 0) & (df['Recipient Type'] != 'BTC') & (
                df['Payout Currency'].isin(['GBP', 'EUR'])), 'Client USD'] = \
                df['Payout Amount'] * df['Client Rate']

            # as in final.xlsx
            # df.loc[(df['Recipient Type'] == 'BTC') & (df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'Client USD'] = \
            #     df['Pay-in Amount'] * df['Client Rate']
            #
            # df.loc[(df['Recipient Type'] != 'BTC') & (df['Payout Currency'].isin(['GBP', 'EUR'])), 'Client USD'] = \
            #     df['Payout Amount'] * df['Client Rate']

        # df['Payout rate']
        # print(382, df.columns)
        selector = (
            self.fiat_selector(df) &
            self.non_african_to_african_selector(df) &
            (
                self.app_is_new_selector(df) |
                self.bitpesa_selector(df)
            )
        )
        df['Client Rate'] = df['Client Rate'].astype('float')
        df['Client USD type'] = ''
        df.loc[selector, 'Client USD'] = df['Client Rate'] * df['Pay-in Amount'] / df['Payout Rate']
        df.loc[selector, 'Client USD type'] = "(Client rate * Pay-in Amount)/Payout Currency rate in MTM"

        """
        if app == 'Bitpesa V2' and AFRICAN CURRENCY >> NON AFRICAN CURRENCY:
            client usd =  (Client rate * Pay Out Amount)/Payin Currency rate in MTM
        none - same as payin > payout
        """

        """
        if app == 'bipesa v2' and (AFRICAN CURRENCY >> AFRICAN CURRENCY):
            if payin < payout:
                client usd = (Client rate * PayIn Amount) / Payout Currency rate in MTM
            else:
                client usd = (Client rate * Payout Amount)/Payin Currency rate in MTM
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            (
                self.african_to_non_african_selector(df) |
                self.african_to_african_selector(df)
            )
        )
        sel = selector & (df['Pay-in Amount'] < df['Payout Amount'])
        df['Client USD type'] = ''
        df.loc[sel, 'Client USD'] = df['Client Rate'] * df['Pay-in Amount'] / df['Payout Rate']
        df.loc[sel, 'Client USD type'] = "(Client rate * PayIn Amount) / Payout Currency rate in MTM"

        sel = selector & (df['Pay-in Amount'] > df['Payout Amount'])
        df.loc[sel, 'Client USD'] = df['Client Rate'] * df['Payout Amount'] / df['Pay-in Rate']
        df.loc[sel, 'Client USD type'] = "(Client rate * Payout Amount)/Payin Currency rate in MTM"

        """
        IF APP name == 'Mukuru' then
            client USD=Payout Amount (Column K)/Client Rate (Col AD)
        """
        mukuru_sel = (df['App'] == 'Mukuru')
        df.loc[mukuru_sel, 'Client USD'] = df['Payout Amount'] / df['Client Rate']
        df.loc[mukuru_sel, 'Client USD type'] = "Mukuru, Payout Amount/Client Rate"
        return df

    def append_m2m_rates(self, df):
        df['Time Paid Out'] = pd.to_datetime(df['Time Paid Out']).dt.date
        df = pd.merge(df,
                      self.rates_mtm[
                          full_currency_list + ["Time Paid Out"]],
                      on='Time Paid Out',
                      how='left')
        return df

    def calc_mtm_rate(self, df, calc_payin_eur=False):
        """
        IF(Recipient Type<>"BTC";DVP(Time Paid Out;'All rates'!A:G;
        IF('Cleaned Data'!Payout Currency="KES";3;
        IF('Cleaned Data'!S2="NGN";2;
        IF('Cleaned Data'!S2="UGX";5;
        IF('Cleaned Data'!S2="TZS";4;
        IF('Cleaned Data'!S2="CNY";7))))));

        ВПР(Y2;'All rates'!A:G;
        IF('Cleaned Data'!Pay-in Currency="KES";3;
        IF('Cleaned Data'!Pay-in Currency="NGN";2;
        IF('Cleaned Data'!Pay-in Currency="UGX";5;
        IF('Cleaned Data'!Pay-in Currency="TZS";4;
        IF('Cleaned Data'!Pay-in Currency="CNY";7)))))))

        # new
        1. Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        =Lookup rate MTM rate (
        If PAYIN = GBP/EUR/USD then lookup rate in GBP/EUR/ USD
        If PAYOUT = NGN then lookup rate in NGN (Column D)
        Final rate = MTM GBP or EUR or USD * NGN rate

        2) Bitpesa V2 (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        If APP name = Bitpesa V2 then
        =Lookup rate MTM rate (
        If PAYIN = GBP/EUR/USD then lookup rate in GBP/EUR/ USD
        If PAYOUT = NGN then lookup rate in NGN (Column D)
        Final rate = MTM GBP or EUR or USD * NGN rate

        3) Bitpesa V2 (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        =Lookup rate MTM rate (
        If PAYIN = NGN then lookup rate in GBP/EUR/ USD
        If PAYOUT = EUR then lookup rate in NGN (Column D)e
        Final rate = MTM rate e.g. EUR * NGN rate

        4) Bitpesa V2 (AFRICAN CURRENCY >> AFRICAN CURRENCY)
        =Lookup rate MTM rate (
        If PAYIN = NGN then lookup rate MTM sheet.
        If PAYOUT = UGX then lookup rate in NGN (Column D)
        Final rate = MTM rate e.g. Bigger rate (UGX rate) / Smaller rate (NGN rate)


        ### in code
        # if new app or bitpesa:
            mtm_rate = payin rate * payout rate

        if bitpesa:
            bigger mtm rate / smaller mtm rate

        :param self:
        :param df:
        :return:
        """
        self.logger.info('Calc MTM Rate column')
        df['MTM Rate'] = 0

        # Prepare mtm_rates
        if not 'NGN' in df.columns:
            df = self.append_m2m_rates(df)

        for currency in full_currency_list:
            df.loc[(df['Recipient Type'] != 'BTC') & (df['Payout Currency'] == currency), 'MTM Rate'] = df[currency]
            df.loc[(df['Recipient Type'] == 'BTC') & (df['Pay-in Currency'] == currency), 'MTM Rate'] = df[currency]

        """
        # if new app or bitpesa:
        mtm_rate = mtm_payin * mtm_payout
        """
        selector = (
            (
                self.fiat_selector(df) &
                self.app_is_new_selector(df) &
                self.non_african_to_african_selector(df)
            ) |
            (
                self.fiat_selector(df) &
                self.bitpesa_selector(df) &
                (
                    self.african_to_non_african_selector(df) |
                    self.non_african_to_african_selector(df)
                )
            )
        )
        df['MTM Rate type'] = ''
        df.loc[selector, 'MTM Rate'] = df['Pay-in Rate'] * df['Payout Rate']
        df.loc[selector, 'MTM Rate type'] = "mtm_payin * mtm_payout"

        """
        if bitpesa and african_to_african:
            bigger mtm rate / smaller mtm rate
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            self.african_to_african_selector(df)
        )
        sel = selector & (df['Pay-in Rate'] > df['Payout Rate'])
        df.loc[sel, 'MTM Rate'] = df['Pay-in Rate'] / df['Payout Rate']
        df.loc[sel, 'MTM Rate type'] = "mtm_payin / mtm_payout"

        sel = selector & (df['Pay-in Rate'] < df['Payout Rate'])
        df.loc[sel, 'MTM Rate'] = df['Payout Rate'] / df['Pay-in Rate']
        df.loc[sel, 'MTM Rate type'] = "mtm_payout / mtm_payin"

        return df

    def calc_mtm_usd(self, df, calc_payin_eur=False):
        """
        =IF(Recipient Type<>"BTC";Payout Amount/MTM Rate;Pay-in Amount/MTM Rate)
        
        # from cleaned_data4
        IF "Recipient Type" = "BTC" and "PAYIN" = EUR or GBP, then =  "Payout Amount"*"MTM Rate"
        ElseIF "Recipient Type" IS NOT "BTC" and "PAYOUT" = EUR or GBP, then =  "Pay-in Amount"*"MTM Rate"
        ElseIF "Recipient Type" = "BTC" then =  "Payout Amount"/"MTM Rate"
        ElseIF "Recipient Type" is NOT  "BTC" then =  "Pay-in Amount"/"MTM Rate"
        
        =
        # from 20171015_final2.xlsx
        # IF(OR(Pay-In:Pay-in Currency="EUR";Pay-In:Pay-in Currency="GBP");MTM Rate*Pay-in Amount;
        # IF(and(OR(Payout Currency:Payout Currency="EUR";Payout Currency:Payout Currency="GBP");Pay-In<>"BTC";MTM Rate*Pay-in Amount);Payout Amount*MTM Rate;
        # IF(Pay-In<>"BTC";Payout Amount/MTM Rate;Pay-in Amount/MTM Rate)))


        # new
        1. Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        =PAYIN amount * MTM rate

        2) Bitpesa V2 (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        =PAYIN amount * MTM rate /Payout Currency rate in MTM

        3) Bitpesa V2 (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        =Payout amount * MTM rate /Payin Currency rate in MTM

        4) Bitpesa V2 (AFRICAN CURRENCY >> AFRICAN CURRENCY)
        When Payin < Payout amount e.g. NGN  to UGX
                =Payin amount * MTM rate /Payout Currency rate in MTM

        IF MUKURU
        MTM USD  (column AG = Payout Amount (Column K)/MTM Rate (Col AF)

        When Pain > Payout amount  e.g NGN to ZAR
                =Payout amount * MTM rate /Payin Currency rate in MTM
        :param df:
        :return:
        """

        self.logger.info('Calc MTM USD')
        df['MTM USD'] = 0

        # Payout Amount*MTM Rate

        df.loc[(df['Recipient Type'] != 'BTC') & (df['MTM USD'] == 0), 'MTM USD'] = df['Payout Amount'] / df[
            'MTM Rate']

        df.loc[(df['Recipient Type'] == 'BTC') & (df['MTM USD'] == 0), 'MTM USD'] = \
            df['Pay-in Amount'] / df['MTM Rate']

        if calc_payin_eur:
            # df.loc[(df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'MTM USD'] = df['Pay-in Amount'] * df['MTM Rate']
            #
            # df.loc[(df['Recipient Type'] != 'BTC') & (df['Payout Currency'].isin(['GBP', 'EUR'])), 'MTM USD'] = \
            #     df['Pay-in Amount'] * df['MTM Rate']
            # df.loc[(df['Recipient Type'] == 'BTC') & (df['Payout Currency'].isin(['GBP', 'EUR'])), 'MTM USD'] = \
            #     df['Payout Amount'] * df['MTM Rate']

            # as in instruction
            df.loc[(df['Recipient Type'] == 'BTC') & (df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'MTM USD'] = \
                df['Pay-in Amount'] * df['MTM Rate']

            df.loc[(df['Recipient Type'] != 'BTC') & (df['Payout Currency'].isin(['GBP', 'EUR'])), 'MTM USD'] = \
                df['Payout Amount'] * df['MTM Rate']

        """
        1. Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
            =PAYIN amount * MTM rate (GBP/EUR/USD) depending on the pain currency
        2) Bitpesa V2 (NON AFRICAN CURRENCY >> AFRICAN CURRENCY) 
        =PAYIN amount * MTM rate /Payout Currency rate in MTM

        """
        selector = (
            self.fiat_selector(df) &
            (
                self.app_is_new_selector(df) |
                self.bitpesa_selector(df)
            ) &
            self.non_african_to_african_selector(df)
        )
        df['MTM USD Type'] = ''
        df.loc[selector, 'MTM USD'] = df['Pay-in Amount'] * df['MTM Rate'] / df['Payout Rate']
        df.loc[selector, 'MTM USD Type'] = "Payin Amount * mtm Rate / payout rate"

        """
        3) Bitpesa V2 (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        =Payout amount * MTM rate /Payin Currency rate in MTM
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            self.african_to_non_african_selector(df)
        )
        df.loc[selector, 'MTM USD'] = df['Payout Amount'] * df['MTM Rate'] / df['Pay-in Rate']
        df.loc[selector, 'MTM USD Type'] = "Payout Amount * MTM Rate / Payin Rate"

        """
                4) Bitpesa V2 (AFRICAN CURRENCY >> AFRICAN CURRENCY)
        When Payin < Payout amount e.g. NGN  to UGX
                =Payin amount * MTM rate /Payout Currency rate in MTM

        When Pain > Payout amount  e.g NGN to ZAR
                =Payout amount * MTM rate /Payin Currency rate in MTM
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            self.african_to_african_selector(df)
        )
        sel = selector & (df['Pay-in Amount'] < df['Payout Amount'])
        df.loc[sel, 'MTM USD'] = df['Pay-in Amount'] * df['MTM Rate'] / df['Payout Rate']
        df.loc[sel, 'MTM USD Type'] = "Payin amount * MTM rate / Payout rate"

        sel = selector & (df['Payout Amount'] < df['Pay-in Amount'])
        df.loc[sel, 'MTM USD'] = df['Payout Amount'] * df['MTM Rate'] / df['Pay-in Rate']
        df.loc[sel, 'MTM USD Type'] = "payout amount * MTM rate /pay-in rate"


        """
        When App name is Mukuru
        MTM USD  (column AG = Payout Amount (Column K)/MTM Rate (Col AF)
        """
        mukuru_sel = (df['App'] == 'Mukuru')
        df.loc[mukuru_sel, 'MTM USD'] = df['Payout Amount'] / df['MTM Rate']
        df.loc[mukuru_sel, 'MTM USD Type'] = "Mukuru, Payout Amount/MTM Rate"

        return df

    def calc_margin_columns(self, df, calc_payin_eur=False):
        """
        margin1
        =IF(Receive Type<>"BTC";(Client USD-MTM USD);(MTM USD- Client USD))
        =IF(Recipient Type<>"BTC";(Client USD-MTM USD);(MTM USD - Client USD)

        margin2
        # from last cleaned
        IF "Recipient Type" = "BTC" and "PAYIN" = EUR or GBP, then =   ("MTM RATE" - "Client Rate")*("MTM USD"/"Client Rate")
        IF "Recipient Type" IS NOT "BTC" and "PAYOUT" = EUR or GBP, then = ("Client Rate" - "MTM RATE")*("MTM USD"/"Client Rate")
        IF "Recipient Type" = "BTC" then =  ("Client Rate" - "MTM RATE")*("MTM USD"/"Client Rate")
        IF "Recipient Type" IS NOT "BTC" then =  ("MTM RATE" - "Client Rate" )*("MTM USD"/"Client Rate")


        from 20171015_final2.xlsx
        =If(AND(OR(Pay-in Currency="GBP";Pay-in Currency="EUR");Pay-in Currency="BTC");(MTM Rate-Client Rate)*MTM USD/Client Rate;
        If(AND(OR(Payout Currency="GBP";Payout Currency="EUR");Pay-In<>"BTC");(Client Rate-MTM Rate)*MTM USD/Client Rate;
        If(Pay-In<>"BTC";(MTM Rate-Client Rate)*MTM USD/Client Rate;(Client Rate-MTM Rate)*MTM USD/Client Rate)))

        # new from march 2018
        margin1
        When Payin < Payout amount e.g. NGN  to UGX
           =  MTM USD-Client USD

        When Payin > Payout amount  e.g NGN to ZAR
           =  Client USD - MTM USD

        ### margin2
        1,2 Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        f.   Margin 2 = ((MTM rate - Client Rate)*Payin Amount * Payin Rate)   / MTM Rate

        3) Bitpesa V2 (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
        f.   Margin 2 = ((Client Rate-MTM rate)*Payout Amount * Payout Rate)   / MTM Rate

        4)
        When Payin < Payout amount e.g. NGN  to UGX
        = ((MTM rate-Client Rate)*Payin Amount) / (MTM Rate * Payin MTM rates )

        When Payin > Payout amount  e.g NGN to ZAR
        = ((Client Rate-MTM rate)*Payout Amount) / (MTM Rate * Payout MTM rates )

        :param df:
        :return:
        """
        # margin 1
        self.logger.info('Calc margin1')
        df['MARGIN 1'] = 0
        # IF "Recipient Type" IS NOT "BTC" then =  "Client USD" - "MTM USD"
        # IF "Recipient Type" = "BTC" then =  "MTM USD" - "Client USD"
        df.loc[df['Recipient Type'] != 'BTC', 'MARGIN 1'] = df['Client USD'] - df['MTM USD']
        df.loc[df['Recipient Type'] == 'BTC', 'MARGIN 1'] = df['MTM USD'] - df['Client USD']

        """
        margin1
        When Payin < Payout amount e.g. NGN  to UGX
           =  MTM USD-Client USD

        When Payin > Payout amount  e.g NGN to ZAR
           =  Client USD - MTM USD
        """
        selector = (
            self.fiat_selector(df) &
            (
                self.bitpesa_selector(df) |
                self.app_is_new_selector(df)
            )
        )

        def margin_calc(c):
            if c['Pay-in Amount'] < c['Payout Amount']:
                res = c['MTM USD'] - c['Client USD']
            else:
                res = c['Client USD'] - c['MTM USD']
            return res

        sel = (selector) & (df['Pay-in Amount'] < df['Payout Amount'])
        df['MARGIN 1 Type'] = ''
        df['MARGIN 2 Type'] = ''
        df.loc[sel, 'MARGIN 1'] = df['MTM USD'] - df['Client USD']
        df.loc[sel, 'MARGIN 1 Type'] = 'MTM USD - Client USD'

        sel = (selector) & (df['Pay-in Amount'] > df['Payout Amount'])
        df.loc[sel, 'MARGIN 1'] = df['Client USD'] - df['MTM USD']
        df.loc[sel, 'MARGIN 1 Type'] = 'Client USD - MTM USD'

        # margin 2
        self.logger.info('Calc margin2')
        df['MARGIN 2'] = 0

        """
        IF "Recipient Type" = "BTC" then =  ("Client Rate" - "MTM RATE")*("MTM USD"/"Client Rate")
        IF "Recipient Type" IS NOT "BTC" then =  ("MTM RATE" - "Client Rate" )*("MTM USD"/"Client Rate")
        """
        df.loc[df['Recipient Type'] == 'BTC', 'MARGIN 2'] = \
            ((df['Client Rate'] - df['MTM Rate']) * df['MTM USD']) / df['Client Rate']
        df.loc[df['Recipient Type'] != 'BTC', 'MARGIN 2'] = \
            ((df['MTM Rate'] - df['Client Rate']) * df['MTM USD']) / df['Client Rate']

        # only for second leg transactions
        if calc_payin_eur:
            # df.loc[(df['Recipient Type'] == 'BTC') & (df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'MARGIN 2'] = \
            #     (df['MTM Rate'] - df['Client Rate']) * df['MTM USD'] / df['Client Rate']
            # df.loc[(df['Recipient Type'] != 'BTC') & (df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'MARGIN 2'] = \
            #     (df['Client Rate'] - df['MTM Rate']) * df['MTM USD'] / df['Client Rate']
            df.loc[(df['Recipient Type'] == 'BTC') & (df['Pay-in Currency'].isin(['GBP', 'EUR'])), 'MARGIN 2'] = \
                (df['MTM Rate'] - df['Client Rate']) * df['MTM USD'] / df['Client Rate']
            df.loc[(df['Recipient Type'] != 'BTC') & (df['Payout Currency'].isin(['GBP', 'EUR'])), 'MARGIN 2'] = \
                (df['Client Rate'] - df['MTM Rate']) * df['MTM USD'] / df['Client Rate']

        """
        primary is Mondial
        1,2 Primary or Transfer zero, Remitpal, LycaRemit, Exchange4Free (NON AFRICAN CURRENCY >> AFRICAN CURRENCY)
        f.   Margin 2 = ((MTM rate - Client Rate)*Payin Amount * Payin Rate)   / MTM Rate
        """
        selector = (
            self.fiat_selector(df) &
            (
                self.app_is_new_selector(df) |
                self.bitpesa_selector(df)
            ) &
            self.non_african_to_african_selector(df)
        )
        df.loc[selector, 'MARGIN 2'] = (
                                           (df['MTM Rate'] - df['Client Rate']) * df['Pay-in Amount'] * df[
                                               'Pay-in Rate']) / \
                                       df['MTM Rate']
        df.loc[selector, 'MARGIN 2 Type'] = "((MTM rate - Client Rate)*Payin Amount * Payin Rate)   / MTM Rate"

        """
        3) Bitpesa V2 (AFRICAN CURRENCY >> NON AFRICAN CURRENCY)
            f.   Margin 2 = ((Client Rate-MTM rate)*Payout Amount * Payout Rate)   / MTM Rate
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            self.african_to_non_african_selector(df)
        )
        df.loc[selector, 'MARGIN 2'] = ((df['Client Rate'] - df['MTM Rate']) * df['Payout Amount']
                                        * df['Payout Rate']) / df['MTM Rate']
        df.loc[selector, 'MARGIN 2 Type'] = "_(((Client Rate - MTM rate)*Payout Amount * Payout Rate)   / MTM Rate"

        """
        When Payin < Payout amount e.g. NGN  to UGX
        = ((MTM rate-Client Rate)*Payin Amount) / (MTM Rate * Payin MTM rates )

        When Payin > Payout amount  e.g NGN to ZAR
        = ((Client Rate-MTM rate)*Payout Amount) / (MTM Rate * Payout MTM rates )
        """
        selector = (
            self.fiat_selector(df) &
            self.bitpesa_selector(df) &
            self.african_to_african_selector(df)
        )
        sel = (selector) & (df['Pay-in Amount'] < df['Payout Amount'])
        df.loc[sel, 'MARGIN 2'] = ((df['MTM Rate'] - df['Client Rate']) * df['Pay-in Amount']) / (
            df['MTM Rate'] * df['Pay-in Rate'])
        df.loc[sel, 'MARGIN 2 Type'] = "((MTM rate-Client Rate)*Payin Amount) / (MTM Rate * Payin MTM rates )"

        sel = (selector) & (df['Pay-in Amount'] > df['Payout Amount'])
        df.loc[sel, 'MARGIN 2'] = ((df['Client Rate'] - df['MTM Rate']) * df['Payout Amount']) / (
            df['MTM Rate'] * df['Payout Rate'])
        df.loc[sel, 'MARGIN 2 Type'] = "((Client Rate-MTM rate)*Payout Amount) / (MTM Rate * Payout MTM rates )"
        return df

    def apply_colors(self, df, writer, sheet_name):
        # add styles
        workbook = writer.book
        worksheet = writer.sheets[sheet_name]
        # AY
        colors = {
            'Exchange Rate (BTC/USD) - Copy': '#9CC3E4',
            'Client Rate': '#72AC4D',
            'Client USD': '#72AC4D',
            'MTM Rate': '#FDBF2D',
            'MTM USD': '#FDBF2D',
            'MARGIN 1': '#EB7D37',
            'MARGIN 2': '#5E9CD3',
        }
        for column, color in colors.items():
            excel_column = excel_style_col(df.columns.get_loc(column) + 2)
            self.logger.debug('setting for column {} - {} color {}'.format(column, excel_column, color))
            format = workbook.add_format({'bg_color': color})
            worksheet.set_column('{}:{}'.format(excel_column, excel_column), 15, format)

    def get_month_filtered_df(self, df, yr_month):
        if not 'yr_mnth' in df.columns:
            df['yr_mnth'] = pd.to_datetime(self.df['Time Started']).dt.date.apply(lambda x: x.strftime('%Y-%m'))
        filtered_df = df[df['yr_mnth'] == yr_month]
        return filtered_df

    def save_results(self):
        self.logger.info('Saving margin results to xlsx')
        # save all info to one xls file with different sheetnames
        self.df['yr_mnth'] = pd.to_datetime(self.df['Time Started']).dt.date.apply(lambda x: x.strftime('%Y-%m'))
        # self.margin_df['yr_mnth'] = self.margin_df['Time Started'].apply(lambda x: x.strftime('%Y-%m'))
        self.original_df['yr_mnth'] = pd.to_datetime(self.original_df['Time Started']).apply(lambda x: x.strftime('%Y-%m') if x else '')
        if self.second_leg_df is not None:
            self.second_leg_df['yr_mnth'] = pd.to_datetime(self.second_leg_df['Time Started']).apply(lambda x: x.strftime('%Y-%m'))

        from_to_str = self._get_dates_str()

        for yr_mnth in self.df['yr_mnth'].unique():
            output_file = 'output/margin/margin_{}{}.xlsx'.format(from_to_str, yr_mnth)
            self.logger.info('saving to {}'.format(output_file))
            writer = pd.ExcelWriter(output_file, engine='xlsxwriter')
            lines = [
                # {'df': self.margin_df, 'sheet': 'Margin'},
                {'df': self.df, 'sheet': 'Cleaned Data', 'colors': True},
                {'df': self.original_df, 'sheet': 'Original Data'},
                {'df': self.second_leg_df, 'sheet': 'Deleted FIAT - FIAT transaction', 'colors': True},
            ]
            for line in lines:
                if line['df'] is not None:
                    month_df = self.get_month_filtered_df(line['df'], yr_mnth)
                    month_df.to_excel(writer, line['sheet'])
                    if line.get('colors'):
                        self.apply_colors(month_df, writer, sheet_name='Cleaned Data')
                        # self.apply_colors(self.second_leg_df, writer, sheet_name='Deleted FIAT - FIAT transaction')

            margin_df = self.calc_margin_report(yr_mnth)
            margin_df.to_excel(writer, 'Margin')
            writer.save()
            self.logger.info('saved to {}'.format(output_file))

    def save_csv(self):
        self.logger.info('remove current margin_csv ')
        import os, glob
        for file in glob.glob('output/margin_csv/margin*.csv'):
            os.remove(file)

        self.logger.info('Saving margin results to csv')
        self.df['yr_mnth'] = pd.to_datetime(self.df['Time Started']).dt.date.apply(lambda x: x.strftime('%Y-%m'))
        from_to_str = self._get_dates_str()

        for yr_mnth in self.df['yr_mnth'].unique():
            output_csv = 'output/margin_csv/margin_{}{}.csv'.format(from_to_str, yr_mnth)
            month_df = self.get_month_filtered_df(self.df, yr_mnth)
            month_df.to_csv(output_csv)
            self.logger.info('saved to {}'.format(output_csv))

    def margin_compare(self, fin_file='report_example/20170912_final.xlsm'):
        self.logger.info("Comparing margin results with example file.")

        df = self.df
        self.logger.debug('Comparing Clearned data sheet')
        fi = pd.read_excel(fin_file, sheetname='Cleaned Data')

        # TODO remove me
        # fi = fi[~(fi['Pay-in Currency'] == 'XOF')]
        # df = df[~(df['Pay-in Currency'] == 'XOF')]

        if df.shape[0] == fi.shape[0]:
            self.logger.info('len is same - {}'.format(df.shape[0]))
        else:
            self.logger.error('len is not same. df.shape - {}, fi.shape - {}'.format(df.shape[0], fi.shape[0]))

        # self.compare(column_name='Recipient Type', df=df, fi=fi)
        self.compare(column_name='Payout Amount', df=df, fi=fi)
        self.compare(column_name='Pay-in Amount', df=df, fi=fi)
        self.compare(column_name='Exchange Rate (BTC/USD)', df=df, fi=fi)
        self.compare(column_name='Exchange Rate (BTC/USD) - Copy', df=df, fi=fi)
        self.compare(column_name='Client Rate', df=df, fi=fi)
        self.compare(column_name='Client USD', df=df, fi=fi)
        self.compare(column_name='MTM Rate', df=df, fi=fi)
        self.compare(column_name='MTM USD', df=df, fi=fi)
        self.compare(column_name='MARGIN 1', df=df, fi=fi)
        self.compare(column_name='MARGIN 2', df=df, fi=fi)


        self.logger.debug('Comparing Deleted FIAT-FIAT data sheet')
        fi_d = pd.read_excel(fin_file, sheetname='Deleted FIAT - FIAT transaction')
        df_d = self.second_leg_df

        if df.shape[0] == fi.shape[0]:
            self.logger.info('len is same - {}'.format(df_d.shape[0]))
        else:
            self.logger.info('len is not same. df.shape - {}, fi.shape - {}'.format(df_d.shape[0], fi_d.shape[0]))
        self.compare(column_name='Exchange Rate (BTC/USD) - Copy', df=df_d, fi=fi_d)
        self.compare(column_name='Client Rate', df=df_d, fi=fi_d)
        self.compare(column_name='Client USD', df=df_d, fi=fi_d)
        self.compare(column_name='MTM Rate', df=df_d, fi=fi_d)
        self.compare(column_name='MTM USD', df=df_d, fi=fi_d)
        self.compare(column_name='MARGIN 1', df=df_d, fi=fi_d)
        self.compare(column_name='MARGIN 2', df=df_d, fi=fi_d)
        self.compare(column_name='Difference', df=df_d, fi=fi_d)

    def compare(self, column_name, df, fi, key_column="Payout Number Ref", is_round=True):
        # self.logger.debug('')
        # self.logger.debug('Compare {} with final'.format(column_name))
        # print(df[df.duplicated(['Key'])])

        # merge by key_column
        # pd.merge(df, rates[['Closing mark', "Time Paid Out"]], on='Time Paid Out',
        #                       how='left')
        fi[key_column] = fi[key_column].astype('str')
        df[key_column] = df[key_column].astype('str')

        # fi = fi[~(fi['Pay-in Currency'] == 'XOF')]
        # df = df[~(df['Pay-in Currency'] == 'XOF')]

        if is_round:
            fi[column_name] = round(fi[column_name].astype('float'), 3)
            df[column_name] = round(df[column_name].astype('float'), 3)

        combined = pd.merge(df[[column_name, key_column]],
                            fi[[column_name, key_column]],
                            on=key_column,
                            how='left'
                            )
        diff = combined[combined[column_name + '_x'] != combined[column_name + '_y']]
        if diff.shape[0] == 0:
            self.logger.debug('Compare {} with final...'.format(column_name) + 'success all lines ident')
        else:
            self.logger.info('Compare {} with final'.format(column_name))
            self.logger.error('{} lines Not ident, example - \n{}'.format(diff.shape[0], diff.head(5)))
            same = combined[combined[column_name + '_x'] == combined[column_name + '_y']]
            # self.logger.info('{} lines ident, example - \n{}'.format(same.shape[0], same.head(3)))
            self.logger.info('{} lines ident'.format(same.shape[0]))

    @staticmethod
    def calc_difference(df, calc_payin_eur=False):
        df['Difference'] = df['MARGIN 2'] - df['MARGIN 1']
        return df

    def clean_columns(self, df, fi=None):
        self.logger.debug('remove columns, that not in finish file')
        # if fi:
        #     fi_columns = list(fi.columns)  # commented because we can not have the final document for compare
        # else:
        fi_columns = ['UUID', 'Sender', 'Sender Type', 'Sender Email', 'Sender Country', 'Recipient', 'Pay-in Currency',
                      'Recipient Type', 'Receive Type', 'Recipient Phone', 'Recipient Country', 'Reference Amount',
                      'Reference Currency', 'Intermediate Amount Due', 'Pay-in Amount',
                      'Payout Amount Expected', 'Payout Amount', 'Payout Currency', 'USD Equivalent', 'State',
                      'Time Started', 'Time Paid Out', 'Payout Number Ref', 'Enterprise Payment', 'Year',
                      'Managed Wallet', 'Payin Provider', 'Payout Provider', 'Bulk', 'OTC', 'Internal Treasury',
                      'Broker', 'Trade Date', 'Settlement Date', 'BTC Amount', 'Fiat Source', 'BTC Source',
                      'Referral Source', 'Referral Code', 'Fiat Currency', 'External ID', 'App',
                      'Exchange Rate (BTC/USD)', 'Sender Copy',
                      'Exchange Rate (BTC/USD) - Copy', 'Client Rate', 'Client USD', 'MTM Rate', 'MTM USD',
                      'MARGIN 1', 'MARGIN 2', 'Difference', 'CORRIDOR', 'Segments', 'Date Started']

        df_columns = list(df.columns)
        union_columns = [column for column in fi_columns if column in df_columns]
        self.logger.debug('unioun columns - {}'.format(union_columns))

        self.logger.debug('removed_columns - {}'.format(set(df_columns) - set(union_columns)))
        df = df[union_columns]
        return df

    def calc_margin_report(self, yr_mnth):
        """
        Volume
        =SUM('Cleaned Data'!MTM USD:MTM USD)

        Cleaned Margin Calculation
        =SUM('Cleaned Data'!MARGIN 1:MARGIN 1)

        Magin from deleted side
        =SUM('Deleted FIAT - FIAT transaction'!MARGIN 1:MARGIN 1)

        IDT fees
        =COUNTIF('Cleaned Data'!App:App;"Money Transfer")*IDT FEES(1.5)

        Aftab Fees
        =COUNTIF('Cleaned Data'Sender == "AFTAB CURRENCY EXCHANGE LIMITED")*Aftab Fees(1.2)
        =COUNTIF('Cleaned Data'!C:C;"AFTAB CURRENCY EXCHANGE LIMITED")*ВПР(A6;J3:K5;2;ЛОЖЬ)

        Gross Margin
        =SUM(D3:D6)
        summ(Cleaned Margin Calculation + Magin from deleted side + IDT fees + Aftab Fees)

        Gross Margin (%)
        =D7/D2
        =Gross Margin * 100% / Volumne


        AFTAB CURRENCY

        SUMIF('Cleaned Data'Sender == "AFTAB CURRENCY EXCHANGE LIMITED";'Cleaned Data'!'MTM USD':'MTM USD')
        SUMIF('Cleaned Data'!C:C;B12;'Cleaned Data'!BC:BC)

        jnfx_fee
        =C12*-0,005
        =AFTAB CURRENCY*-0,005



        :return:
        """
        lines = []
        df = self.df
        df_d = self.second_leg_df
        if yr_mnth:
            df = self.get_month_filtered_df(df, yr_mnth)
            if df_d is not None:
                df_d = self.get_month_filtered_df(df_d, yr_mnth)

        def make_line(name, com1='', com2='', val=None, round_nd=None):
            if round_nd is None:
                round_nd = 0

            if isinstance(val, float):
                val = round(val, round_nd)

            max_date = str(df['Time Paid Out'].max())
            dic = {
                'Name': name,
                'c1': com1,
                'c2': com2,
                max_date: val
            }
            self.logger.debug('Calc {} - {}'.format(name, val))
            return dic

        const = {
            'idt_fees': 1.5,
            'aftab_fees': 1.2,
        }

        max_date = str(df['Time Paid Out'].max())
        volume = df['MTM USD'].sum()
        margin_calc = df['MARGIN 1'].sum()
        margin_from_deleted_side = df_d['MARGIN 1'].sum() if df_d is not None else 0
        idt_fees = df[df["App"] == 'Money Transfer']['UUID'].count() * const['idt_fees']
        aftab_fees = df[df["Sender"] == "AFTAB CURRENCY EXCHANGE LIMITED"]['UUID'].count() * const['aftab_fees']
        gross_margin = margin_from_deleted_side + margin_calc + idt_fees + aftab_fees
        gross_margin_p = gross_margin * 100 / volume

        aftab_currency_mtm_usd_sum = df[df["Sender"] == "AFTAB CURRENCY EXCHANGE LIMITED"]['MTM USD'].sum()
        jnfx_fee = aftab_currency_mtm_usd_sum * -0.005

        lines.append(make_line('Volume', val=volume))
        lines.append(make_line('Cleaned Margin Calculation', val=margin_calc))
        lines.append(make_line('Magin from deleted side', val=margin_from_deleted_side))
        lines.append(make_line('IDT fees', val=idt_fees))
        lines.append(make_line('Aftab Fees', val=aftab_fees))
        lines.append(make_line('Gross Margin', val=gross_margin))
        lines.append(make_line('Gross Margin (%)', val=gross_margin_p, round_nd=2))
        lines.append(make_line('', val=''))
        lines.append(make_line('', val=''))
        lines.append(make_line('', val=''))
        lines.append(make_line('JNFX (Fee)', com1='AFTAB CURRENCY EXCHANGE LIMITED', com2=aftab_currency_mtm_usd_sum,
                               val=jnfx_fee))
        res = pd.DataFrame(lines)
        margin_df = res[['Name', 'c1', 'c2', max_date]]
        return margin_df


if __name__ == '__main__':
    import sys
    cd = MarginClean()
    # print(sys.)
    if 'testing' in sys.argv[1:]:
        cd.margin(testing=True, compare=True)
    elif 'dates' in sys.argv[1:]:
        date_from = input('Enter a date from in YYYY-MM-DD format or skip for user date_form=begin\n').strip() or None
        date_to = input('Enter a date to in YYYY-MM-DD format or skip for use date_to=now\n').strip() or None
        cd.margin(date_from=date_from, date_to=date_to)
    elif 'only_csv' in sys.argv[1:]:
        cd.margin(only_csv=True)
    else:
        # cd.margin(date_from='2018-02-01', date_to='2018-02-01')
        # cd.margin(only_csv=True)
        cd.margin()
