# coding=utf-8
import datetime
import glob
import html
import os
import sys

import numpy as np
import pandas as pd
if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

from constants import segments_list, other_currency, africa_currency, rate_sheet_aps
from logger import MyLogger


class CleanData(MyLogger):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.df = None
        self.original_df = None
        self.rates = None
        self.sender_column = "Sender"
        self.fiat_idt_senders = ["IDT"]
        self.allow_months = None  # None means any

    def _show_shape(self, df=None, comment=''):
        # if df is not None:
        #     df = self.df
        if df is None:
            self.logger.info('shape is unnown, df is None')
        else:
            self.logger.info('shape - {}, comment-{}'.format(df.shape, comment))


    def prepare_dates(self, **kwargs):
        self.logger.info('starting prepare dates')
        from dateutil.relativedelta import relativedelta
        try:
            date_from_default = '2009-01-01'
            date_to_default = datetime.datetime.now().strftime("%Y-%m-%d")
            s_date_from = kwargs.get('date_from')
            s_date_to = kwargs.get('date_to')
            if s_date_from in [None, '']:
                s_date_from = date_from_default
            if s_date_to in [None, '']:
                s_date_to = date_to_default
            date_from = datetime.datetime.strptime(s_date_from, "%Y-%m-%d")
            date_to = datetime.datetime.strptime(s_date_to, "%Y-%m-%d")
        except:
            raise Exception('Some error with date_from and date_to. Use format YYYY-MM-DD')

        count_of_months = (date_to.year - date_from.year) * 12 + date_to.month - date_from.month
        allow_months = []
        for i in range(count_of_months + 1):
            allow_months.append((date_from + relativedelta(months=i)).strftime('%Y-%m'))
        self.allow_months = allow_months
        self.date_from = date_from.date()
        self.date_to = date_to.date()
        self.logger.info('Date_from=%s, date_to=%s, allow_months=%s' % (date_from, date_to, allow_months))


    def idt_selector(self, df):
        """
        Selector for choose only IDT lines
        :param df:
        :return:
        """
        return ((df[self.sender_column].isin(self.fiat_idt_senders)) | (df['App'] == 'ACE'))

    def clean(self, filename=None, filenames=None, save_results=True, **kwargs):
        self.prepare_dates(**kwargs)

        # get input data
        self.get_data(filename, filenames)

        self.initial_preparations()
        self.data_reclassification()
        self.clean_duplicates()
        self.double_fiat_fiat_not_IDT()
        self.double_fiat_fiat_IDT()
        self.recalculate_volume()
        self.calc_corridor_column()
        self.make_segments()

        self.change_recepient()
        self.checking()

        if save_results:
            self.save_results()

    def _get_dates_str(self):
        from_to_str = ''
        days_from = (datetime.datetime.now().date() - self.date_from).days
        days_to = (self.date_to - datetime.datetime.now().date()).days
        if not (days_from > 1000 and days_to == 0):
            from_to_str = "{}_{}_".format(self.date_from, self.date_to)
            self.logger.info('from_to_str - {}'.format(from_to_str))
        return from_to_str

    def save_results(self):
        """
        Split all results to months and save each month to separate file
        :return:
        """
        self.logger.info('remove current report files')
        import os, glob
        for file in glob.glob('output/cleaned/*.csv'):
            os.remove(file)

        self.logger.info('saving results')
        self.df['yr_mnth'] = self.df['Date Started'].apply(lambda x: x.strftime('%Y-%m'))
        self.logger.info(self.df['yr_mnth'].unique())

        # filename = 'output.csv'
        # self.df.to_csv('output/cleaned/{}'.format(filename))
        # self.logger.info('saved {} rows to output/cleaned/{}'.format(self.df.shape[0], filename))

        # special mixin for filename for cases, when we set from or to.
        from_to_str = self._get_dates_str()

        for yr_mnth in self.df['yr_mnth'].unique():
            # self.df[self.df['yr_mnth'] == yr_mnth].to_csv('output/cleaned/{}.csv'.format(yr_mnth))
            # self.logger.info('saved {} rows to output/cleaned/{}.csv'.format(
            #     self.df[self.df['yr_mnth'] == yr_mnth].shape[0], yr_mnth))
            #
            filename = 'output/cleaned/{}{}.csv'.format(from_to_str, yr_mnth)
            self.df[self.df['yr_mnth'] == yr_mnth].to_csv(filename)
            self.logger.info('saved {} rows to {}'.format(self.df[self.df['yr_mnth'] == yr_mnth].shape[0], filename))

    def get_data(self, filename=None, filenames=None):
        """
        Read data from filename or filenames, if both is blank - then read direcotory input
        :param filename: the name of file for read info
        :param filenames: the list of names of files for read info
        :return:
        """
        if filename is None and filenames is None:
            filenames = self.get_filenames(self.allow_months)
        elif filename:
            filenames = [filename]
        for filename in filenames:
            self.read_file(filename=filename)
        good_columns = ['UUID', 'Sender', 'Sender Type', 'Sender Email', 'Sender Country', 'Recipient',
                        'Pay-in Currency',
                        'Recipient Type', 'Receive Type', 'Recipient Phone', 'Recipient Country', 'Reference Amount',
                        'Reference Currency', 'Intermediate Amount Due', 'Pay-in Amount',
                        'Payout Amount Expected', 'Payout Amount', 'Payout Currency', 'Exchange Rate (BTC/USD)',
                        'USD Equivalent', 'Trade Price', 'State', 'Time Started', 'Time Paid Out',
                        'Payout Number Ref', 'Enterprise Payment', 'Year', 'Managed Wallet', 'Payin Provider',
                        'Payout Provider', 'Bulk', 'OTC', 'Internal Treasury', 'Broker', 'Trade Date',
                        'Settlement Date', 'BTC Amount', 'Fiat Source', 'BTC Source', 'Referral Source',
                        'Referral Code', 'Fiat Currency', 'External ID', 'App']
        # self.df = self.df[good_columns]

        if self.date_from or self.date_to:
            # If we set date_from or date_to then we filter only part of choosen df
            self.df.loc[:, "Date Started"] = pd.to_datetime(self.df['Time Started'], errors='coerce').dt.date
            self.logger.info('filtering by choosen dates from=%s, to=%s, len-%s' % (self.date_from, self.date_to, self.df.shape[0]))
            date_selector = (self.df['Date Started'] >= self.date_from) & \
                            (self.df['Date Started'] <= self.date_to)

            self.df = self.df[date_selector]
            self.logger.info('after_filtering len = %s' % self.df.shape[0])

        self.original_df = self.df  # [good_columns]
        self.df["Sender"] = self.df['Sender'].str.replace("&amp;", "&")
        self.logger.info('new shape - {}'.format(self.df.shape))
        self.read_rates()

    def get_filenames(self, allow_months=None):
        """
        Read  'Input' directory and get names of csv files
        allow_months - ['2017-12', '2018-01'] list of allowed months
        :return: list of *.csv files
        """
        self.logger.info("Search *.csv files in input folder")
        self.logger.info("allow_months = %s" % allow_months)
        filenames = []
        directory = "input"
        os.chdir(directory)
        for filename in glob.glob("*.csv"):
            if not '_fixed' in filename:
                filenames.append(os.path.join(directory, filename))
        os.chdir("../")
        self.logger.info("found files: {}".format(filenames))

        # filter filenames
        if allow_months:
            cleaned_filenames = []
            for filename in filenames:
                if not '-' in filename:
                    cleaned_filenames.append(filename)
                else:
                    for month in allow_months:
                        if month in filename:
                            cleaned_filenames.append(filename)
            filenames = cleaned_filenames
            self.logger.info("cleaned files: {}".format(filenames))

        return filenames

    def read_file(self, filename='data/dc.csv'):
        """
        Read file and concat info to self.df
        :param filename:
        :return:
        """
        if '.csv' in filename:
            old_filename = filename[:]
            # filename = filename.replace('.', '_fixed.')
            # with open(old_filename, 'r+') as f:#, open(filename, 'w') as g:
            #     content = f.read()
            # content = html.unescape(content)
            # f.write(content)

        self.logger.info('Reading from file {}'.format(filename))
        if '.csv' in filename:
            import pandas as pd
            import codecs
            with codecs.open(filename, "r", encoding='utf-8', errors='replace') as fdata:
                content = fdata.read()
                content = html.unescape(content).strip()
                max_lines = None
                content = "\n".join(content.split('\n')[:max_lines])

                df = pd.read_csv(StringIO(content), index_col=False)

                if not 'Pay-in Amount' in df.columns:
                    df = self._change_df_to_new_style(df)

            self.logger.info("shape - {}".format(df.shape))
            if self.df is None:
                self.df = df
            else:
                self.df = pd.concat([self.df, df])
        elif '.xls' in filename:
            self.df = pd.read_excel(filename)
            # self.sender_column = "Sender.1"
        self.df['index'] = np.arange(len(self.df))
        self.df = self.df.set_index('index')

        self.logger.debug('Reading finished, shape - {}'.format(self.df.shape))

    @staticmethod
    def _change_df_to_new_style(df):
        columns = {
            'Intermediate Amount Paid': 'Pay-in Amount',
            'Pay-In': 'Pay-in Currency',
            'Payout Amount Sent': 'Payout Amount',
            'Payout Reference Number': 'Payout Number Ref',
        }
        df.rename(columns=columns, inplace=True)
        return df

    def read_rates(self):
        self.logger.info('Reading MTM_rates from input/rates.xlsx')
        rates_mtm = pd.read_excel('input/rates.xlsx', sheetname='MTM Rates')
        rates_mtm = rates_mtm.rename(columns=lambda x: x.strip())  # clean spaces from column names
        rates_mtm = rates_mtm[rates_mtm['Date'].str.contains(':', na=True)]
        rates_mtm['Date'] = pd.to_datetime(rates_mtm['Date'], errors='ignore').dt.date
        rates_mtm['Closing mark'] = rates_mtm['NGN.1']
        rates_mtm['NGN'] = rates_mtm['NGN.1']
        rates_mtm['Time Paid Out'] = rates_mtm['Date']

        # self.logger.info('Reading IDT_rates from input/rates.xlsx')
        rates_idt = pd.read_excel('input/rates.xlsx', sheetname='IDT rates')
        rates_idt = rates_idt.rename(columns=lambda x: x.strip())  # clean spaces from column names
        rates_idt['IDT RATES'] = pd.to_datetime(rates_idt['IDT RATES']).dt.date
        rates_idt['Date'] = rates_idt['IDT RATES']
        rates_idt['Time Paid Out'] = rates_idt['Date']

        self.logger.info('Reading IDT_rates from input/rates.xlsx')
        aftab_rates = pd.read_excel('input/rates.xlsx', sheetname='AFTAB')
        aftab_rates = aftab_rates.rename(columns=lambda x: x.strip())  # clean spaces from column names
        aftab_rates['IDT RATES'] = pd.to_datetime(aftab_rates['IDT RATES']).dt.date
        aftab_rates['Date'] = aftab_rates['IDT RATES']
        aftab_rates['AFTAB NGN'] = aftab_rates['NGN']
        aftab_rates['Time Paid Out'] = aftab_rates['Date']

        # special sheets:

        fiat_rates = None
        for app, app_dict in rate_sheet_aps.items():
            temp_df = pd.read_excel('input/rates.xlsx', sheetname=app_dict.get('sheet'))
            temp_df.head()

            def clean_columns(columns):
                good_columns = [columns[0]] + [column for column in columns if '/' in column]
                return good_columns

            temp_df = temp_df[clean_columns(temp_df.columns)]
            temp_df.columns = ['Date Started'] + ["%s_%s" % (app, c.split(' ')[0]) for c in temp_df.columns[1:]]
            temp_df['Date Started'] = pd.to_datetime(temp_df['Date Started']).dt.date
            if fiat_rates is None:
                fiat_rates = temp_df
            else:
                fiat_rates = pd.merge(fiat_rates, temp_df, on='Date Started', how='left')


        result = pd.merge(rates_mtm[['Date', 'Closing mark']], rates_idt[['IDT RATES', 'NGN', "UGX", 'Date']],
                          on='Date', how='left')
        result = pd.merge(result[['Date', 'Closing mark', 'IDT RATES', 'NGN', "UGX"]],
                          aftab_rates[['AFTAB NGN', 'Date']],
                          on='Date', how='left')
        self.rates = result
        self.rates_idt = rates_idt
        self.rates_mtm = rates_mtm
        self.rates_aftab = aftab_rates
        self.fiat_rates = fiat_rates
        self.logger.debug("Get rates {}".format(self.rates.shape[0]))

    def initial_preparations(self):
        """
        Create column for date of completion. Calculated based on "Time Paid Out" column.
        Duplicate Sender column to have a copy of original names
        Remove from calculation if recipeint is "BT Pesa Nigeria LTD"
        """
        self.logger.info('Start initial_preparations')
        df = self.df
        # Create column for date of completion. Calculated based on "Time Paid Out" column.
        null_time_out = df['Time Paid Out'].isnull()
        self.logger.debug("removing rows with TimePaidOut is Null - {}".format(df[null_time_out].shape[0]))
        df = df[~null_time_out]
        df = df[~(df[self.sender_column].isnull())]
        df.loc[:, "Date Started"] = pd.to_datetime(df['Time Started'], errors='coerce').dt.date
        df.loc[:, "Date Completion"] = pd.to_datetime(df['Time Paid Out'], errors='coerce').dt.date

        # Duplicate Sender column to have a copy of original names
        df.loc[:, 'Sender Copy'] = df[self.sender_column]
        self.logger.debug('Made sender copy')

        # Remove from calculation if recipeint is "BT Pesa Nigeria LTD"
        sel = df["Recipient"].str.contains("BT Pesa Nigeria", na=False)
        df = df[~sel]

        # Remove all refunds in "STATE" column
        sel = df["State"].str.contains("refunded", na=False)
        self.logger.debug('remove refunds, shpe - {}'.format(df[sel].shape))
        df = df[~sel]

        # Remove from calculation if 'Payout Amount' == ‘0’
        # self.logger.debug("remove 'Payout Amount' == ‘0’, shape - {}".format(df[df['Payout Amount'] == 0].shape[0]))
        bad_uuid = df[df['Payout Amount'] == 0]['UUID']
        payout_sent_selector = df['UUID'].isin(bad_uuid)
        self.logger.debug("remove 'Payout Amount' == ‘0’, shape - {}".format(df[payout_sent_selector].shape[0]))
        df = df[~payout_sent_selector]

        self.df = df
        self.logger.info('result shape is {}'.format(df.shape))

        self.df['fiat'] = (df['Pay-in Currency'] != 'BTC') & (df['Payout Currency'] != 'BTC')

        # If app is Mukuru and both is NGN - then change payin to USD
        mukuru_sel = (df['App'] == 'Mukuru') & (df['Pay-in Currency'] == 'NGN')
        self.logger.debug('mukuru has %s lines' % df[mukuru_sel].shape[0])
        df.loc[mukuru_sel, 'Pay-in Currency'] = 'USD'

        self.logger.debug('finish initial_preparations')

    def data_reclassification(self):
        """
        Reclassify All ACE transactions
                  - Search for all ACE transactions in APPs column and Rename Individual senders to AFTAB CURRENCY EXCHANGE LIMITED
                  - In "Sender Type" Change Person" to Business
        Reclassify All IDT transactions
                  - Search for all IDT transactions  and Rename Individual senders to IDT
                  - In "Sender Type" Change Person" to Business
        Reclassify All Transfer Zero transactions
                  - Search for all Transfer Zero transactions  and Rename Individual senders to Transfer Zero
                  - In "Sender Type" Change Person" to Business
        Reclassify All Christian Ikedue  transactions
                  - Search for all Christian Ikedue transactions  and Rename Individual senders to Exchange4Free
                  - In "Sender Type" Change Person" to Business
        Reclassify All Soko  transactions
                  - Search for all ella griffith transactions  and Rename Individual senders to Soko
                  - In "Sender Type" Change Person" to Business
        Reclassify All ASA Oil and Gas transactions
                  - Search for all "ABDURRAHMAN SANI ADAM" transactions  and Rename Individual senders to "ASA oil & gas"
                  - In "Sender Type" Change Person" to Business

        Combine "VIP Express Tourism Ltd" with "Nowahala Tours"
        :return:
        """
        self.logger.info("Start data_reclassification")
        df = self.df
        search = [["App", "Money Transfer", "IDT"],
                  ["App", "TransferZero", "Transfer Zero"],
                  ["App", "Transfer zero", "Transfer Zero"],
                  ["App", "ACE", "AFTAB CURRENCY EXCHANGE LIMITED"],
                  ["App", "Hello Group", "Daytona"],
                  ["App", "exchange4free", "Exchange4free Ltd"],
                  ["App", "Bitwala", "Bitwala"],
                  ["App", "SP","SimbaPay"],
                  ["App", "Primary", "Mondial"],
                  ["App", "Bitbond","Bitbond Gmbh"],
                  ["App", "Mukuru","Mukuru"],
                  [self.sender_column, "ella griffith", "Soko"],
                  [self.sender_column, "ABDURRAHMAN SANI ADAM", "Adam Sani (ASA Oil & Gas)"],
                  [self.sender_column, "Abel Obazee", "Ceecenta (Abel Obazee)"],
                  [self.sender_column, "Ngozi Dozie", "Cafe Neo (Ngozi Dozie)"],
                  [self.sender_column, "Bamidele Ayemibo", "Bamidele Ayemibo (3TIMPEX)"],
                  [self.sender_column, "Websimba Limited", "WebSimba Limited (EatOut Kenya)"],
                  [self.sender_column, "Thebe Wellness Services", "Thebe Wellness Services Nigeria"],
                  [self.sender_column, "Abel Obazee", "Ceecenta (Abel Obazee)"],
                  ]

        for s in search:
            selector = (df[s[0]].str.contains(s[1], na=False))
            df.loc[selector, self.sender_column] = s[2]
            df.loc[selector, "Sender Type"] = "Business"
            self.logger.debug(
                "data_reclassification {} {} founded".format(df.loc[selector, self.sender_column].shape[0], s[1]))
        df.loc[df[self.sender_column] == "Transfer Zero", 'App'] = 'TransferZero'

        # Combine "VIP Express Tourism Ltd" with "Nowahala Tours
        new_name = "NoWahala Tours/VIP Express Tourism Ltd"
        selector = df[self.sender_column].isin(["VIP Express Tourism Ltd", "Nowahala Tours"])
        self.logger.debug(
            "Find {} rows for combine Nowahala and VIP".format(df[selector].shape[0]))
        df.loc[selector, self.sender_column] = new_name

    def clean_duplicates(self):
        # clean duplicate
        columns_for_search_dupl = None  # ["UUID", "Payout Amount Expected", "Reference Amount", "Recipient Type", "ime Paid Out"]
        self.logger.info(
            "Remove {} duplicated rows".format(self.df[self.df.duplicated(columns_for_search_dupl)].shape[0]))
        self.df = self.df.drop_duplicates(columns_for_search_dupl)
        self.logger.debug(self.df.shape)

    def get_duplicate_UUID(self, df):
        uuid = df.groupby(by=['UUID'])['Sender'].count().reset_index()
        uuid_list = list(uuid[uuid['Sender'] > 1]['UUID'])
        self.logger.info('found %s duplicated UUID' % len(uuid_list))
        return uuid_list

    def double_fiat_fiat_IDT(self):
        """
        In short, all IDT transactions should show USD in PAYIN column in place of NGN/USD
        Example is
        UUID .         PAYIN .                                         RECIPIENT TYPE
        1                  NGN  (Should be USD)                BTC
        1                  BTC                                               NGN::BANK
        2                  UGX  (Should be USD)                BTC
        2                  BTC                                               UGX::BANK

        End result
        Change "BTC" in PAYIN column to USD and delete the row has "BTC" in "Recipient Type" column
        UUID .         PAYIN .                                         RECIPIENT TYPE
        1                  USD                                               NGN::BANK
        2                  USD                                               UGX::BANK

        Example 2: Where
        UUID .         PAYIN .                     RECIPIENT TYPE
        1                  USD                          BTC
        1                  BTC                            NGN::BANK
        2                  GBP                           BTC
        2                  BTC                            UGX::BANK
        3                  EUR                           BTC
        3                  BTC                            KES::BANK

        Replace BTC in the column with  corresponding currency before deleting row with "BTC" in the Recipient type
        UUID .         PAYIN .                     RECIPIENT TYPE
        1                  USD                            NGN::BANK
        2                  GBP                            UGX::BANK
        3                  EUR                            KES::BANK
        :return:
        """
        df = self.df
        self.logger.info('Starting IDT double_fiat_fiat')
        duplicate_uuid_list = self.get_duplicate_UUID(df)

        # outside the continent IDT
        for currency in other_currency:
            uuid_selector = (
            df['UUID'].isin(
                df[
                    (df['Payout Currency'] != 'BTC') & (df['Pay-in Currency'] == 'BTC')
                ]['UUID'])
                & df['UUID'].isin(duplicate_uuid_list)
            )
            selector = ((uuid_selector) & (df['Pay-in Currency'] == currency) & self.idt_selector(df))
            double_uuid_selector = df['UUID'].isin(df[selector]['UUID'])
            self.logger.debug('{} - {}'.format(df[uuid_selector].shape[0], df[double_uuid_selector].shape[0]))
            self.logger.debug("IDT Outside the continent, found {} rows with Pay-In in {}".
                              format(df[selector].shape[0], currency))
            df = df[~selector]
            df.loc[(double_uuid_selector) & (df['Pay-in Currency'] == "BTC"), 'Pay-in Currency'] = "{}".format(currency)

        # africa IDT
        for currency in africa_currency:
            uuid_selector = (
            df['UUID'].isin(df[df['Payout Currency'] != 'BTC']['UUID']) & df['UUID'].isin(duplicate_uuid_list))
            selector = (
                (uuid_selector) & (df['Pay-in Currency'] == currency) & self.idt_selector(df))
            double_uuid_selector = df['UUID'].isin(df[selector]['UUID'])
            self.logger.debug('{} - {}'.format(df[uuid_selector].shape[0], df[double_uuid_selector].shape[0]))
            self.logger.debug("IDT In Africa continent, found {} rows with Pay-In in {}".
                              format(df[selector].shape[0], currency))
            df = df[~selector]
            df.loc[(double_uuid_selector) & (df['Pay-in Currency'] == "BTC"), 'Pay-in Currency'] = "USD"

        # africa IDT for one-side transactions
        for currency in africa_currency:
            uuid_selector = (
            df['UUID'].isin(df[df['Payout Currency'] != 'BTC']['UUID']) & ~df['UUID'].isin(duplicate_uuid_list))
            selector = ((uuid_selector) & (df['Pay-in Currency'] == currency) & self.idt_selector(df))
            self.logger.debug("IDT onse-side In Africa continent, found {} rows with Pay-In in {}".
                              format(df[selector].shape[0], currency))
            df.loc[selector, 'Pay-in Currency'] = "USD"

        self.logger.debug('df.shape - {}'.format(df.shape))
        self.df = df

    def double_fiat_fiat_not_IDT(self):
        """
        NOT IDT
        For intercontinental we agreed to keep one side of the transaction which is as follows:
        UUID          PAYIN       Recipient type           Payout
        1                 USD          BTC                            BTC (REMOVE)
        1                 BTC           NGN::Bank                NGN

        You correctly made the expected changes as shown
        UUID           PAYIN        Recipient type          Payout
        1                   USD          NGN::Bank                NGN


        For intracontinental we agreed to keep one side of the transaction which is as follows:
        UUID          PAYIN       Recipient type         PAYOUT
        1                 KES          BTC                           BTC
        1                 BTC           NGN::Bank               NGN  (REMOVE)

        You correctly made the expected changes as shown
        UUID           PAYIN        Recipient type   PAYOUT
        1                   KES          BTC                     NGN

        :return:
        """
        df = self.df
        self.logger.info('NOT IDT Starting double_fiat_fiat')
        duplicate_uuid_list = self.get_duplicate_UUID(df)

        # outside the continent NOT IDT
        for currency in other_currency:
            uuid_selector = (
            df['UUID'].isin(
                df[
                    (df['Payout Currency'] != 'BTC') & (df['Pay-in Currency'] == 'BTC')
                ]['UUID'])
                & df['UUID'].isin(duplicate_uuid_list)
            )
            selector = ((uuid_selector) & (df['Pay-in Currency'] == currency) & (~self.idt_selector(df)))
            double_uuid_selector = df['UUID'].isin(df[selector]['UUID'])
            # self.logger.debug('{} - {}'.format(df[uuid_selector].shape[0], df[double_uuid_selector].shape[0]))
            self.logger.debug("NOT IDT Outside the continent, found {} rows with Pay-In in {}".
                              format(df[selector].shape[0], currency))
            df = df[~selector]
            # double_uuid_selector = df['UUID'].isin(df[selector]['UUID'])
            df.loc[(double_uuid_selector) & (df['Pay-in Currency'] == "BTC"), 'Pay-in Currency'] = "{}".format(currency)

        # Africa NOT IDT
        africa_payin = df['Pay-in Currency'].isin(africa_currency)

        # select all rows, where UUID same with africa currency in Pay-In
        uuid_selector = (df['UUID'].isin(df[africa_payin]['UUID']) & df['UUID'].isin(duplicate_uuid_list))

        # select rows, where Pay-In == 'BTC' and UUID same with rows where Pay-In = africa_currency
        selector = ((uuid_selector) & (df['Pay-in Currency'] == 'BTC') & (~self.idt_selector(df)))

        # select rows, who have same UUID for africa currency and BTC
        double_uuid_selector = df['UUID'].isin(df[selector]['UUID'])

        self.logger.debug('selector len - {}, {}'.format(df[selector].shape[0],
                                                         df[double_uuid_selector].shape[0]))

        for payout_currency in list(df[selector]['Payout Currency'].unique()):
            # get selector, where Pay-in Currency='BTC' and payout = selected payout currency
            # and have africa currency in payin for same UUID
            af = (selector) & (df['Payout Currency'] == payout_currency)

            # get all rows for this selector + rows with same UUID
            short_uuid_selector = df['UUID'].isin(df[af]['UUID'])
            self.logger.info('2e10180b-f055-449a-a80a-8890019f2337' in df[short_uuid_selector]['UUID'])

            # choose only rows with recepient type == 'BTC'
            short_sel = ((short_uuid_selector) & (df['Recipient Type'] == 'BTC'))

            self.logger.debug(
                "NOT IDT Find inafrica {} rows with {} as payout for ".format(df[short_sel].shape[0], payout_currency))
            # and set payout currency for this rows to selected payout currency.
            df.loc[short_sel, 'Payout Currency'] = payout_currency
        df = df[~selector]

        self.logger.debug('df.shape - {}'.format(df.shape))
        self.df = df

    def recalculate_volume(self):
        """
         All Nigerian Transactions (Excluding "IDT")
        -When PAYIN currency is "NGN", Replace transactions-2017-06 (27) (Clomn "Exchange
        Rate (BTC/USD)") with a vlookup value of a new new rate provided in Sheet("Rates")
         column D("Closing Mark") and calculate  the new Volume. i.e.  "Intermediate Amount
          Paid"/"Exchange Rate (BTC/USD)"

        You mean:
        For all payments, when PAYIN = "NGN" and Sender != "IDT"
        Change column "Exchange Rate (BTC/USD)" to value from Sheet("Rates") column D for date of payment.
        And then Calculate Value(Is it new column?) as "Pay-in Amount" / "Exchange Rate"
        Example:
        Row:f666ffd2-8a8f-4614-9733-c6c87550662d
         date:6/29/2017
         Pay-in Amount = 30002
         Rate from Column D for 6/29/2017 = 366

        So I have to change column Exchange Rate to 366
        and Calc Value = 30 002 / 366 = 81,9
        It is strange result, that's why I want be sure, that i get you right.
        :return:
        """
        self.logger.info("Recalculate Volume start")

        df = self.df
        rates = self.rates
        if rates is None:
            self.logger.error('No rates')
            return ""

        # get rate for exchange_rate column
        rates['Date'] = pd.to_datetime(rates["Date"]).dt.date
        rates['Time Paid Out'] = rates['Date']
        df['Time Paid Out'] = pd.to_datetime(df['Time Paid Out'], errors='coerce').dt.date

        self.logger.info('For NOT IDT')
        df = pd.merge(df, rates[['Closing mark', "Time Paid Out", 'AFTAB NGN']], on='Time Paid Out',
                      how='left')

        def calc_value(selector, df, exhange_column, paid_column="Pay-in Amount"):
            """
            Calc USD Equivalent for selector rows based on exhange_column
            :param selector:
            :param df:
            :param exhange_column:
            :return:
            """
            selector = ((selector) & ~(df[exhange_column].isnull()))
            df.loc[selector, "Exchange Rate (BTC/USD)"] = df[selector][exhange_column]

            df = df.drop([exhange_column], axis=1)
            self.logger.debug("Exchange Rate (BTC/USD) calculated")

            # Calculate Value(Is it new column?) as "Pay-in Amount" / "Exchange Rate"
            df[paid_column] = df[paid_column].astype('float')
            df[selector]["Exchange Rate (BTC/USD)"] = df[selector]["Exchange Rate (BTC/USD)"].astype('float')
            df.loc[selector, "USD Equivalent"] = round(df[selector][paid_column].astype(np.double) /
                                                       df[selector]["Exchange Rate (BTC/USD)"].astype(np.double), 2)
            self.logger.info(
                df[selector][["UUID", "Exchange Rate (BTC/USD)", 'Time Paid Out', "USD Equivalent"]].head(2))
            self.logger.debug("Value calculated for {} rows".format(df[selector].shape[0]))
            return df

        selector = ((df["Pay-in Currency"] == 'NGN') & (~self.idt_selector(df)))
        df = calc_value(selector, df, 'Closing mark')


        self.logger.info('For IDT rows')
        rates['IDT RATES'] = pd.to_datetime(rates["IDT RATES"]).dt.date
        rates['Time Paid Out'] = rates['IDT RATES']

        df = pd.merge(df, rates[["NGN", "UGX", "Time Paid Out"]], on='Time Paid Out',
                      how='left')

        self.logger.info('aftab calc')
        selector = ((df["Payout Currency"] == 'NGN') & (df['App'] == "ACE"))
        df = calc_value(selector, df, 'AFTAB NGN', 'Payout Amount')

        self.logger.debug('NGN')
        selector = ((df['Payout Currency'] == 'NGN') & (df[self.sender_column] == "IDT"))
        df = calc_value(selector, df, 'NGN', 'Payout Amount')

        self.logger.debug('UGX')
        selector = ((df['Payout Currency'] == 'UGX') & (df[self.sender_column] == "IDT"))
        df = calc_value(selector, df, 'UGX', 'Payout Amount')

        df[selector].head()
        self.df = df

    def calc_corridor_column(self):
        """
        Create a column called Corridor and combine currency in PAYIN column with that in PAYOUT Column.
        Example
        UUID              PAYIN               PAYOUT        CORRIDOR
        1                     USD                  NGN              USD : NGN
        2                     GBP                  KES               GBP : KES
        3                     BTC                  NGN              BTC : NGN.      Etc
        :return:
        """
        self.logger.info("Single Column for Currency start")
        df = self.df

        def get_coridor(row):
            return row['Pay-in Currency'] + ' : ' + row['Payout Currency']

        df['CORRIDOR'] = df.apply(lambda row: get_coridor(row), axis=1)

    def make_segments(self):
        """
        Create a column called “Segments”.
        Fill in with this information:
        If sender type = “Person” then Segments = “HIGH VOLUME BITCOIN TRADERS IN AFRICA”

        :return:
        """
        self.logger.info("Make Segment column")

        df = self.df
        df["Segments"] = ""
        self.logger.debug("For segment HIGH VOLUME BITCOIN TRADERS IN AFRICA we find {} rows".format(
            df[df["Sender Type"] == "Person"].shape[0]
        ))
        df.loc[df["Sender Type"] == "Person", "Segments"] = "HIGH VOLUME BITCOIN TRADERS IN AFRICA"

        for seg in segments_list:
            lower_senders = [sen.lower() for sen in seg['senders']]
            selector = ((df["Sender Type"] == "Business") & (df[self.sender_column].str.lower().str.strip().isin(lower_senders)))

            self.logger.debug("For segment {} we have {} senders and find {} rows".format(
                seg["segment_name"], len(seg['senders']), df[selector].shape[0]
            ))

            df.loc[selector, "Segments"] = seg['segment_name']

    def checking(self):
        """
        some auto-checking that all is fine
        :return:
        """
        self.logger.info("Self checking CLEAN")
        df = self.df
        # all segments should be filled
        check = {
            'cond': df[df["Segments"] == ''].shape[0] == 0,
            'info_message': "Count of rows with blank segment should be 0",
            'success_message': "Success, no blank segments",
            'fail_message': "Blank segments for this senders: \n - {}".format(
                "\n - ".join(list(df[df["Segments"] == ''][self.sender_column].unique()))
            )
        }
        self.error_check(**check)

        # all segments should be filled
        check = {
            'cond': df[df["State"] == 'refunded'].shape[0] == 0,
            'info_message': "State == refunded should be 0",
            'success_message': "Success, no refunded lines",
            'fail_message': "Refunded lines: \n - {}".format(
                "\n - ".join(list(df[df["State"] == 'refunded']["Payout Number Ref"].head()))
            )
        }
        self.error_check(**check)


        check = {
            'cond': df[df["USD Equivalent"] == 0].shape[0] == 0,
            'info_message': "Count of rows with blank USD should be 0",
            'success_message': "Success, no blank USD Equivalent",
            'fail_message': "There are some lines with USD Equivalent == 0: \n - {}".format(
                "\n - ".join(list(df[df["USD Equivalent"] == 0]["Payout Number Ref"].head()))
            )
        }
        self.error_check(**check)

        # IDT Pay-In should be only USD
        check = {
            'cond': list(df[self.idt_selector(df)]['Pay-in Currency'].unique()) == ['USD'],
            'info_message': "IDT Pay-In should be only USD",
            'success_message': "Success, all IDT Pay-In is USD",
            'fail_message': "Fail, Pay-in is {}. example = {}".format(
                df[self.idt_selector(df)]['Pay-in Currency'].unique(),
                df[self.idt_selector(df) & (df['Pay-in Currency'] != 'USD')]['UUID'].head())
        }
        self.error_check(**check)

        # check UUID-Payin duplicates
        check = {
            'cond': len(df["UUID"].unique()) == len(df.groupby(by=["UUID", 'Pay-in Currency'])),
            'info_message': "check UUID-payin duplicated",
            'success_message': "Success, No UUID-Payin duplicates",
            'fail_message': "Fail, {} duplicated, Example - {}".format(
                (len(df["UUID"].unique()) == len(df.groupby(by=["UUID", 'Pay-in Currency']))),
                df.groupby(by=["UUID", 'Pay-in Currency']).sum().reset_index()['UUID'].value_counts().head())
        }
        self.error_check(**check)



    def error_check(self, cond, info_message, success_message, fail_message):
        self.logger.info("")
        self.logger.info(info_message)
        if cond:
            self.logger.info(success_message)
        else:
            self.logger.error(fail_message)

    def change_recepient(self):
        self.logger.info('startig to change recepient for non african payin')
        africa_selector = self.df['Pay-in Currency'].isin(africa_currency)
        self.df.loc[africa_selector, 'Recipient Type'] = "BTC"
        self.logger.info('finished')



if __name__ == '__main__':
    # import sys
    # print('asdf, arg - {}'.format(sys.argv[1:]))
    cd = CleanData()
    cd.clean()
    # cd.clean(date_from='2018-04-07', date_to='2018-04-07')
    # print(cd.prepare_dates())
    # print(cd.prepare_dates(date_from='2017-12-01', date_to='2017-02-28'))
    # print(cd.prepare_dates())
    # print(cd._get_dates_str())
    # filenames = cd.get_filenames(cd.allow_months)
    # print(814, filenames)
    # cd.get_data(filenames=filenames)
