# Install #
* Install programs  
```sh
python3 https://www.python.org/downloads/  
git https://git-scm.com/download/
```

* prepare virtualenv
```
__path_python__/scripts/pip install virtualenv  
__path_python__/scripts/virtualenv venv
. venv/scripts/activate
```

* Alternative way for prepare script, don't need admin perm for mac.
```
Download and install anaconda for python 3.6 https://www.anaconda.com/download/ (it can take 5-10 minutes)  
restart computer
conda create --name py36 python=3.6 anaconda
source activate py36
```

* git clone
```
git clone https://arhangel66@bitbucket.org/arhangel66/data_cleaning.git
cd data_cleaning
pip install -r requirements.txt
```

* prepare folder with input files
```
input/rates.xlsx  
#in special_format(!). please check rates_example.xlsx. Columns and sheet names are important.
# for clean and report.py
 
input/transactions*.csv 
# It can be several files. For all scripts

input/Combined Data.xlsx
# For report.py
```

* run the script
```
python clean.py
python report.py  # require run clean.py at first use files 'output/clean/' as a base
python margin.py
# for testing:
python margin.py testing
```

* Results

You will find results in folder 'output':  
'output/cleaned/' for clean.py script   
'output/margin/' for margin script  
'output/report' for report.py script  

# Margin
Margin script make many calculations, based on instructions from jeffrey.
The heart of script is function 'def margin'. It contains all main steps of script (each step is one new column).
It use Clean script as a base, and then call margin function.
So it is:
1. Same as clean script read csv files like 'input/transactions-2017-06.csv' from input folder
2. clean it and move results to margin function.
3. Calculate columns with functions (calc_client_rate, calc_client_usd, calc_mtm_rate, calc_mtm_usd, calc_margin_columns, calc_difference) each function has copy of formula from instruction in it's head.
4. Save result to xlsx file with some formatiing to output/margin/margin.xlsx

#### Margin, development mode
If you will start script with command python manage.py testing it will call the script in special model.
It will use as input file 'input/margin_check/20171015_input2.xlsx' and will compare results with file 'input/margin_check/20171015_final2.xlsm'. + will write some extra columns to output file for check calculations, for example, what formula was used for calc 'user rate' column.

#### Rates 
rates should have exact format, as in this file. Column names is importnant. Sheet names is important. Column ordering - not important (only names).


### Answer to some questions
**How to add new currency, like XOF to MTM rates?**  
it is easy:  
- check, that you have column with this name in rates.xlsx file, 'MTM Rates' sheet. http://take.ms/GY0j6
- Add new currency to constants.py http://take.ms/zHw3f
that's all. It will automatically implement for clean.py and margin.py (for mtm rate file)
- run commands for push your new code to code repository:  
```git
git pull
git commit -m 'Jeffrey fix'
git push
```

**How to add new currency for IDT? or special currency for some APPs, like AFTAB?**  
It is not easy and now it will require some programmer work.

**How to add new segment?**  
In constants.py file. when you change any file - you need to push code to repository:
```git
git pull
git commit -m 'Jeffrey fix'
git push
```

**How to start script using jupiter notepad?**  
from clean import CleanData
CleanData.clean()

In case of any issue, question or request for change you can write to arhangel662@gmail.com